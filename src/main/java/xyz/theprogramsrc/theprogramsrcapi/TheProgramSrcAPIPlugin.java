package xyz.theprogramsrc.theprogramsrcapi;


import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.programmingUtils.MathUtil;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;
import java.util.concurrent.TimeUnit;



import org.bukkit.plugin.java.JavaPlugin;

public final class TheProgramSrcAPIPlugin extends JavaPlugin{



    public static TheProgramSrcClass tps;

    @Override
    public void onEnable(){
        Utils.sendMessage(this.getServer().getConsoleSender(), "&f&lTPSAPI&b&l>> &r&a&lEnabling TheProgramSrcAPI...");
        tps = new TheProgramSrcClass(this, "TPSAPI","TPS","&bTPS>&7", "es", true,false,true,false);
        Utils.runDelayedTask(()->Utils.sendMessage(this.getServer().getConsoleSender(), "&f&lTheProgramSrcAPI&b&l>> &r&a&lEnabled TheProgramSrcAPI successfully!"), MathUtil.fairRoundedRandom(0,7), TimeUnit.SECONDS);
    }

    @Override
    public void onDisable(){
        Utils.sendMessage(this.getServer().getConsoleSender(), "&f&lTPSAPI&b&l>> &r&a&lDisabling TheProgramSrcAPI...");
        Utils.runDelayedTask(()->Utils.sendMessage(this.getServer().getConsoleSender(), "&f&lTheProgramSrcAPI&b&l>> &r&a&lDisabled TheProgramSrcAPI successfully!"), MathUtil.fairRoundedRandom(0,4), TimeUnit.SECONDS);
    }


}
