package xyz.theprogramsrc.tpsapi.ownOptions.commands;

import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.gameManagement.commands.ICommand;
import xyz.theprogramsrc.tpsapi.gameManagement.commands.Result;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

public class getUUIDCommand extends ICommand{

    public getUUIDCommand(TheProgramSrcClass theProgramSrcClass){
        super(theProgramSrcClass, "getuuid");
    }

    @Override
    public Result onPlayerExecute(Player player, String[] args){
        if(args.length == 0){
            Utils.sendMessage(player, "&bUUID> &7"+player.getUniqueId().toString());
            Utils.sendMessage(player, "&bUUID> &7" + (getTPS().getLanguageManager().getPhrase("write-getuuid-cmd-to-copy")));
        }else{
            if(args[0].toLowerCase().equalsIgnoreCase("copy")){
                StringSelection selection = new StringSelection(player.getUniqueId().toString());
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(selection, selection);
            }
        }
        return Result.COMPLETED;
    }

    @Override
    public Result onConsoleExecute(CommandSender sender, String[] args){
        return Result.NOT_SUPPORTED;
    }
}
