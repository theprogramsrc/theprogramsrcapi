package xyz.theprogramsrc.tpsapi.ownOptions.files;

import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.generalManagement.files.CustomFile.CustomFile;

import java.io.File;

public class PluginFile extends CustomFile{

    private String cachedPrefix;

    public PluginFile(TheProgramSrcClass theProgramSrcClass, String defaultPrefix){
        super(theProgramSrcClass,false,true);
        this.setDefaultPrefix(defaultPrefix);
    }

    @Override
    public void onFileReload(){
        this.cachedPrefix = null;
    }

    public void setDefaultPrefix(String prefix){
        this.add("Prefix", prefix);
        this.save();
    }

    public String getPrefix(){
        if(this.cachedPrefix == null){
            this.cachedPrefix = this.getString("Prefix");
        }
        return this.cachedPrefix;
    }

    @Override
    public String getFileName(){
        return this.getPluginName();
    }

    @Override
    public String getFileExtension(){
        return ".yml";
    }

    @Override
    public File getFileFolder(){ return this.getMainFolder(); }
}
