package xyz.theprogramsrc.tpsapi.ownOptions.files;

import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.generalManagement.files.CustomFile.CustomFile;

import java.io.File;

public class Config extends CustomFile{

    public Config(TheProgramSrcClass theProgramSrcClass, String defaultLanguage){
        super(theProgramSrcClass,false,false);
        this.setHeader(getLanguageManager().getPhrase("here-you-can-modify-plugin-config"), getLanguageManager().getPhrase("available-languages"),"- Spanish", "- English");
        this.add("Language", defaultLanguage);
        this.save();
    }

    public String getLanguage(){
        String lang = this.getString("Language");
        return lang.equals("Spanish") ? "es" : "en";
    }

    @Override
    public String getFileName(){ return "Config"; }

    @Override
    public String getFileExtension(){ return ".yml"; }

    @Override
    public File getFileFolder(){ return this.getConfigFolder(); }
}
