package xyz.theprogramsrc.tpsapi.ownOptions.files;

import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.generalManagement.files.CustomFile.CustomFile;
import xyz.theprogramsrc.tpsapi.programmingUtils.mysql.SQLProvider;
import xyz.theprogramsrc.tpsapi.programmingUtils.mysql.SQLSettings;

import java.io.File;

public class MySQLFile extends CustomFile{

    private SQLSettings cache;

    public MySQLFile(TheProgramSrcClass theProgramSrcClass) {
        super(theProgramSrcClass, false,true);
    }

    public void add(SQLProvider provider) {
        SQLSettings s = new SQLSettings(provider.getHost(), provider.getPort(), provider.getDatabase(), provider.getUsername(), provider.getPassword(), provider.isEnabled());
        this.add(s);
    }

    public void add(SQLSettings settings) {
        String p = "SQLSettings";
        if (this.cache == null) {
            this.add(p + ".Enabled", settings.isEnabled());
            this.add(p + ".Host", settings.getHost());
            this.add(p + ".Port", settings.getPort());
            this.add(p + ".Database", settings.getDatabase());
            this.add(p + ".Username", settings.getUsername());
            this.add(p + ".Password", settings.getPassword());
        }

    }

    public SQLSettings getSettings() {
        String p = "SQLSettings";
        if (this.cache == null) {
            boolean en = this.getBoolean(p + ".Enabled");
            String host = this.getString(p + ".Host");
            Integer port = this.getInt(p + ".Port");
            String db = this.getString(p + ".Database");
            String un = this.getString(p + ".Username");
            String pw = this.getString(p + ".Password");
            this.cache = new SQLSettings(host, port, db, un, pw, en);
        }

        return this.cache;
    }

    @Override
    public String getFileName(){
        return "MySQLSettings";
    }

    @Override
    public String getFileExtension(){
        return ".sqlsettings";
    }

    @Override
    public File getFileFolder(){
        return this.getConfigFolder();
    }
}
