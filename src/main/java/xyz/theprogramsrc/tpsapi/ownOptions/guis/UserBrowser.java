package xyz.theprogramsrc.tpsapi.ownOptions.guis;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.gameManagement.items.HeadBuilder;
import xyz.theprogramsrc.tpsapi.gameManagement.items.ItemBuilder;
import xyz.theprogramsrc.tpsapi.gameManagement.items.XMaterial;
import xyz.theprogramsrc.tpsapi.gameManagement.ui.ActionEvent;
import xyz.theprogramsrc.tpsapi.gameManagement.ui.BrowserUI;
import xyz.theprogramsrc.tpsapi.gameManagement.ui.BrowserUIButton;
import xyz.theprogramsrc.tpsapi.gameManagement.ui.UIButton;
import xyz.theprogramsrc.tpsapi.gameManagement.user.User;

import java.util.ArrayList;
import java.util.Arrays;

public abstract class UserBrowser extends BrowserUI<User>{

    private boolean showOffline = false;

    public UserBrowser(TheProgramSrcClass theProgramSrcClass, Player player){
        super(theProgramSrcClass, player);
    }

    @Override
    public void onUILoad(){
        super.onUILoad();
        this.setItem(new UIButton(47, this.getToggleShowOffline(), e->{
            this.showOffline = !this.showOffline;
            this.present();
        }));
    }

    @Override
    public User[] getObjects(){
        User[] users1 = this.getUserManager().getUsers();
        if(!this.showOffline){
            users1 = Arrays.stream(users1).filter(u-> u != null && u.isOnline()).toArray(User[]::new);
        }

        ArrayList<User> r = new ArrayList<>();
        for(User u : users1){
            if(u != null){
                r.add(u);
            }
        }
        User[] users = new User[r.size()];
        users = r.toArray(users);
        return users;
    }

    @Override
    public BrowserUIButton getItem(User u){
        HeadBuilder builder = new HeadBuilder(XMaterial.PLAYER_HEAD, u.getName()).setDisplayName("&a"+u.getName());
        builder.setSkin(u.getSkin());
        return new BrowserUIButton(builder.construct(), e->this.onClick(e, u));
    }

    public abstract void onClick(ActionEvent event, User u);

    private ItemStack getToggleShowOffline(){
        ItemBuilder builder;
        if(this.showOffline){
            builder = new ItemBuilder(XMaterial.ENDER_EYE).setDisplayName(this.isSpanish() ? "&aOcultar Offline" : "&aHide offline").addEnchantment(Enchantment.DURABILITY).showEnchantments(false);
        }else{
            builder = new ItemBuilder(XMaterial.ENDER_PEARL).setDisplayName(this.isSpanish() ? "&aMostrar Offline" : "&aShow Offline");
        }
        return builder.construct();
    }
}
