package xyz.theprogramsrc.tpsapi.programmingUtils.encodeAndDecode;

import org.apache.commons.codec.binary.Base64;

@SuppressWarnings ("unused")
public class Decoder{

    public static String base64(String toDecode){
        byte[] bytes = Base64.decodeBase64(toDecode.getBytes());
        return new String(bytes);
    }
}
