package xyz.theprogramsrc.tpsapi.programmingUtils.encodeAndDecode;

import org.apache.commons.codec.binary.Base64;

@SuppressWarnings ("unused")
public class Encoder{

    public static String base64(String toEncode){
        byte[] bytes = Base64.encodeBase64(toEncode.getBytes());
        return new String(bytes);
    }
}
