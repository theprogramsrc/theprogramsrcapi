package xyz.theprogramsrc.tpsapi.programmingUtils.mysql;

import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

import java.sql.*;

@SuppressWarnings ("ALL")
public class SQL{
    
    private SQLConnection connection;
    
    public SQL(SQLConnection connection){ this.connection = connection; }

    public boolean tableExists(String table) {
        Utils.notNull(table, "Table cannot be Null!");
        try {
            Connection connection = this.connection.getConnection();
            if (connection == null) { return false; }

            DatabaseMetaData metadata = connection.getMetaData();
            if (metadata == null) { return false; }

            ResultSet resultSet = metadata.getTables(null, null, table, null);
            if (resultSet.next()) { return true; }
        } catch (Exception ex) { ex.printStackTrace(); }

        return false;
    }

    public void insertData(String columns, String values, String table) {
        this.connection.executeUpdate("INSERT INTO " + table + " (" + columns + ") VALUES (" + values + ")");
    }

    public void deleteData(String column, String dataType, String data, String table) {
        if (data != null) {
            data = "'" + data + "'";
        }

        this.connection.executeUpdate("DELETE FROM " + table + " WHERE " + column + dataType + data + ";");
    }

    public boolean exists(String column, String data, String table) {
        if (data != null) {
            data = "'" + data + "'";
        }

        try {
            ResultSet rs = this.connection.executeQuery("SELECT * FROM " + table + " WHERE " + column + "=" + data);

            while(rs.next()) {
                if (rs.getString(column) != null) {
                    return true;
                }
            }
        } catch (Exception ex) { ex.printStackTrace(); }

        return false;
    }

    public void deleteTable(String table) { this.connection.executeUpdate("DROP TABLE " + table + ";"); }

    public void truncateTable(String table) { this.connection.executeUpdate("TRUNCATE TABLE " + table + ";"); }

    public void createTable(String table, String columns) {
        if (!tableExists(table)) {
            this.connection.executeUpdate("CREATE TABLE " + table + " (" + columns + ")");
        }
    }

    public void set(String selectedColumn, Object value, String column, boolean equals, String data, String table) {
        if (value != null) { value = "'" + value + "'"; }

        if (data != null) { data = "'" + data + "'"; }

        String dataType = (equals ? " = " : " != ");

        this.connection.executeUpdate("UPDATE " + table + " SET " + selectedColumn + "=" + value + " WHERE " + column + dataType + data + ";");
    }

    public Object get(String selected, String column, boolean equals, String data, String table) {
        if (data != null) { data = "'" + data + "'"; }
        String dataType = (equals ? " = " : " != ");
        try {
            ResultSet rs = this.connection.executeQuery("SELECT * FROM " + table + " WHERE " + column + " " + dataType + data);
            if (rs.next()) { return rs.getObject(selected); }
        } catch (Exception ex) { ex.printStackTrace(); }

        return null;
    }

    public int countRows(String table) {
        int i = 0;
        if (table == null) {
            return i;
        } else {
            ResultSet rs = this.connection.executeQuery("SELECT * FROM " + table);
            try { while(rs.next()) { ++i; } } catch (Exception ex) { ex.printStackTrace(); }
            return i;
        }
    }

    public void addNewColumn(String table, String columnName, String type, String length){
        this.connection.executeUpdate("alter table " + table + " add column " + columnName + " " + type + " ("+length+");");
    }

    public void changeColumnName(String table, String oldColumnName, String newColumnName, String type, String length){
        this.connection.executeUpdate("alter table " + table + " change " + oldColumnName + " " + newColumnName + " " + type + " ("+length+");");
    }

    public void makeBigger(String table, String columnName, String type, String length){
        this.connection.executeUpdate("alter table " + table + " modify " + " " + columnName + " " + type + " ("+length+");");
    }

}
