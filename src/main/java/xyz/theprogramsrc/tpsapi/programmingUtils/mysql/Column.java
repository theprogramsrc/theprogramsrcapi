package xyz.theprogramsrc.tpsapi.programmingUtils.mysql;

@SuppressWarnings ("unused")
public class Column{

    private String columnName;
    private ColumnType columnType;
    private String length;

    public Column(String columnName, ColumnType columnType, String  length){
        this.columnName = columnName;
        this.columnType = columnType;
        this.length = length;
    }

    public String toSQLCommand(){ return this.columnName + " " + columnType.getSQLCmd()+"("+this.length+")"; }
}
