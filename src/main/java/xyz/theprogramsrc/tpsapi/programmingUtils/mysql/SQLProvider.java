package xyz.theprogramsrc.tpsapi.programmingUtils.mysql;

public interface SQLProvider{

    String getHost();

    Integer getPort();

    String getDatabase();

    String getUsername();

    String getPassword();

    boolean isEnabled();
}
