package xyz.theprogramsrc.tpsapi.programmingUtils.mysql;

@SuppressWarnings ("unused")
public enum ColumnType{

    VARCHAR("VARCHAR");

    private String string;

    ColumnType(String string){ this.string = string; }

    public String getSQLCmd(){ return string; }
}
