package xyz.theprogramsrc.tpsapi.programmingUtils.mysql;

import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

public class SQLSettings{

    private String host;
    private Integer port;
    private String database;
    private String username;
    private String password;
    private boolean enabled;

    public SQLSettings(String host, Integer port, String database, String username, String password, boolean enabled){
        Utils.notNull(host,"Host Cannot be null!");
        Utils.notNull(database,"Database Cannot be null!");
        Utils.notNull(username,"Username Cannot be null");
        this.host = host;
        this.port = port != null ? port : 3306;
        this.database = database;
        this.username = username;
        this.password = password != null ? password : "";
        this.enabled = enabled;
    }

    public boolean isEnabled(){ return enabled; }

    public String getHost(){
        return host;
    }

    public Integer getPort(){
        return port;
    }

    public String getDatabase(){
        return database;
    }

    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password;
    }
}
