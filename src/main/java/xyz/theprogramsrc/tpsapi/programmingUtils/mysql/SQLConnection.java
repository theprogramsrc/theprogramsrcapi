package xyz.theprogramsrc.tpsapi.programmingUtils.mysql;

import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;
import xyz.theprogramsrc.tpsapi.*;

import java.sql.*;

@SuppressWarnings ("ALL")
public class SQLConnection extends TPS implements SQLProvider{

    private Connection connection;
    private SQLSettings settings;
    private String host;
    private Integer port;
    private String database;
    private String username;
    private String password;

    public SQLConnection(TheProgramSrcClass theProgramSrcClass, String host, Integer port, String database, String username, String password){
        super(theProgramSrcClass);
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
        this.getSQLFile().add(this);
        this.settings = this.getSQLFile().getSettings();
        this.setConnection(this.settings);
    }

    @Override
    public String getHost(){ return host; }

    @Override
    public Integer getPort(){ return port; }

    @Override
    public String getDatabase(){ return database; }

    @Override
    public String getUsername(){ return username; }

    @Override
    public String getPassword(){ return password; }

    @Override
    public boolean isEnabled(){ return false; }

    public void setConnection(SQLSettings settings){
        if(this.settings.isEnabled()){
            try{
                this.connection = DriverManager.getConnection("jdbc:mysql://"+settings.getHost()+":"+settings.getPort()+"/"+settings.getDatabase(),settings.getUsername(),settings.getPassword());
                this.log(getLanguageManager().getPhrase("connected-successfully"));
            }catch(Exception ex){
                this.log(getLanguageManager().getPhrase("try-with-this-options"));
                this.log(getLanguageManager().getPhrase("check-router-connection"));
                this.log(getLanguageManager().getPhrase("try-reconnect-to-wifi"));
            }
        }
    }

    public Connection getConnection(){ return this.connection; }

    public boolean isConnected(){ return getConnection()!=null; }

    public void connect(){ this.connect(true); }

    public void connect(boolean logMessages){
        if(logMessages){
            try{
                if(isConnected()){
                    this.log(getLanguageManager().getPhrase("cannot-connect-to-db").replace("{EXCEPTION}", getLanguageManager().getPhrase("already-connected")));
                }else{
                    this.setConnection(this.settings);
                }
            }catch(Exception ex){
                this.log(getLanguageManager().getPhrase("cannot-connect-to-db").replace("{EXCEPTION}", ex.getMessage()));
            }
        }else{
            try{
                if(!isConnected()){
                    this.setConnection(this.settings);
                }
            }catch(Exception ex){ ex.printStackTrace(); }
        }
    }

    public void disconnect(){ this.disconnect(true); }

    public void disconnect(boolean logMessages){
        if(logMessages){
            try{
                if(isConnected()){
                    this.connection.close();
                    log(getLanguageManager().getPhrase("disconnected-successfully"));
                }else{
                    log(getLanguageManager().getPhrase("cannot-disconnect").replace("{EXCEPTION}", getLanguageManager().getPhrase("no-existing-connection")));
                }
            }catch(Exception ex){
                log(getLanguageManager().getPhrase("cannot-disconnect").replace("{EXCEPTION}", ex.getMessage()));
            }
        }else{
            try{
                if(isConnected()){
                    this.connection.close();
                }
            }catch(Exception ex){ ex.printStackTrace(); }
        }
    }

    public void executeUpdate(String command){
        Utils.notNull(command,getLanguageManager().getPhrase("command-cannot-be-null"));
        this.connect(false);
        try{
            Statement statement = getConnection().createStatement();
            statement.executeUpdate(command);
            statement.close();
        }catch(Exception ex){
            log(getLanguageManager().getPhrase("mysql-update"));
            log(getLanguageManager().getPhrase("mysql-command").replace("{COMMAND}", command));
            log(getLanguageManager().getPhrase("muysql-error").replace("{EXCEPTION}", ex.getMessage()));
        }
    }

    public ResultSet executeQuery(String command){
        Utils.notNull(command, getLanguageManager().getPhrase("command-cannot-be-null"));
        this.connect(false);
        ResultSet resultSet = null;
        try{
            Statement statement = getConnection().createStatement();
            resultSet = statement.executeQuery(command);
        }catch(Exception ex){
            log(getLanguageManager().getPhrase("mysql-query"));
            log(getLanguageManager().getPhrase("mysql-command").replace("{COMMAND}", command));
            log(getLanguageManager().getPhrase("mysql-error").replace("{EXCEPTION}", ex.getMessage()));
        }

        return resultSet;
    }
}
