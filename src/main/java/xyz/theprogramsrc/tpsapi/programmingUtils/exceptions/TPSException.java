package xyz.theprogramsrc.tpsapi.programmingUtils.exceptions;

@SuppressWarnings ("ALL")
public class TPSException extends RuntimeException{
    public TPSException(String message){ super(message); }
    public TPSException(){ super(); }

    public TPSException(Throwable ex){ super(ex); }
}
