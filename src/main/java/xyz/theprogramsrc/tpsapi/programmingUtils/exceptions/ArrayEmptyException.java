package xyz.theprogramsrc.tpsapi.programmingUtils.exceptions;

@SuppressWarnings ("unused")
public class ArrayEmptyException extends TPSException{

    public ArrayEmptyException(){ super(); }

    public ArrayEmptyException(String message){ super(message); }

}
