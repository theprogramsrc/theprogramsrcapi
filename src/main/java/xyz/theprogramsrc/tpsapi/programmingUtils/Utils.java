package xyz.theprogramsrc.tpsapi.programmingUtils;

import xyz.theprogramsrc.tpsapi.programmingUtils.exceptions.ArrayEmptyException;
import xyz.theprogramsrc.tpsapi.programmingUtils.exceptions.TPSException;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.*;

@SuppressWarnings("ALL")
public class Utils{

    public static boolean nonNull(Object object){
        return object != null;
    }

    public static <T> T requireNonNull(T obj) {
        if (obj == null) throw new NullPointerException();
        return obj;
    }

    public static boolean isEmpty(String string){
        return string.length() == 0;
    }

    public static boolean isEmpty(ArrayList<?> arrayList){
        return arrayList.size() == 0;
    }

    public static boolean isEmpty(List<?> list){
        return list.size() == 0;
    }

    public static boolean isEmpty(Object... objects){
        return objects.length == 0;
    }

    public static boolean isEmpty(String... strings){
        return strings.length == 0;
    }

    public static void Log(Level level, String message){
        Logger.getLogger("TheProgramSrcAPI").log(level, message);
    }

    public static String[] setFirstForAll(String firstForAll, String[] strings){
        ArrayList<String> result = new ArrayList<>();
        for(String s : strings){
            String r = firstForAll + s;
            result.add(r);
        }
        return ((String[]) result.toArray(new String[result.size()]));
    }

    public static String[] setFirstFor(Integer index, String firstFor, String[] strings){
        ArrayList<String> result = new ArrayList<>();
        for(Integer i = 0; i < strings.length; ++i){
            String s = strings[i];
            if(i.equals(index)){
                String r = firstFor + s;
                result.add(r);
            }else{
                result.add(s);
            }
        }
        return ((String[]) result.toArray(new String[result.size()]));
    }

    public static String setFirstFor(String var0, String var1){
        return var0 + var1;
    }

    public static String getEnumName(Enum<?> enumToGetName){
        ArrayList list = new ArrayList();

        for(String enumName : enumToGetName.name().split("_")){
            list.add(enumName.toUpperCase().charAt(0) + enumName.toLowerCase().substring(1));
        }

        return String.join(" ", list);
    }

    public static boolean isInteger(String integer){
        try{
            Integer.parseInt(integer);
            return true;
        }catch(Exception ex){
            return false;
        }
    }

    public static Integer Int(String string){
        return Integer.parseInt(string);
    }

    public static String[] fromList(List<String> list){
        String result = "";
        for(int i = 0; i < list.size(); i++){
            String ltt = list.get(i);
            if(i == 0){
                result = ct(ltt) + "ƒ";
            }else{
                result = result + ct(ltt) + "ƒ";
            }
        }
        String[] r = result.split("ƒ");
        return ct(r);
    }

    public static <T> T notNull(T object){
        return notNull(object, "The object '" + object.getClass().getSimpleName() + "' cannot be null!");
    }

    public static <T> T notNull(T object, String message){
        if(object == null){
            throw new NullPointerException(message);
        }else{
            return object;
        }
    }

    public static <T> T[] notEmpty(T[] array, String message){
        if(array.length == 0){
            throw new ArrayEmptyException(message);
        }else{
            return array;
        }
    }

    public static <T> T[] notEmpty(T[] array){
        return notEmpty(array, "The array " + array.getClass().getName() + " is Empty!");
    }

    public static String notEmpty(String string, String message){
        if(string.length() == 0){
            throw new TPSException(message);
        }else{
            return string;
        }
    }

    public static String notEmpty(String string){
        return notEmpty(string, "The string cannot be null!");
    }

    public static String firstUpperCase(String text){
        return text.substring(0, 1).toUpperCase() + text.substring(1);
    }

    public static String getActualTime(String format){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
        LocalDateTime time = LocalDateTime.now();
        String ha = dtf.format(time);
        return ha;
    }

    public static String ct(String text){
        return text == null ? "" : ChatColor.translateAlternateColorCodes('&', text);
    }

    public static ArrayList<String> ct(ArrayList<String> list){
        ArrayList<String> result = new ArrayList<>();
        list.forEach(s -> result.add(s != null ? (!s.equals(" ") ? ct(s) : " ") : " "));
        return result;
    }

    public static List<String> ct(List<String> list){
        return ct(new ArrayList<>(list));
    }

    public static String[] ct(String[] array){
        ArrayList<String> arrayList = new ArrayList<>();
        for(String s : array){
            arrayList.add(s != null ? (!s.equals(" ") ? ct(s) : " ") : " ");
        }
        return ((String[]) arrayList.toArray(new String[arrayList.size()]));
    }

    public static void sendMessage(CommandSender sender, String message){
        sender.sendMessage(ct(message));
    }

    public static String randomAlphaNumeric(int lenght){
        String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder builder = new StringBuilder();
        while(lenght-- != 0){
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public static <T> T[] toArray(T... array){
        return array;
    }

    public static String[] toStringArray(String string){
        string = removeCharAt(0, string);
        string = removeCharAt(string.toCharArray().length, string);
        return string.split(", ");
    }

    public static String removeCharAt(int index, String s){
        StringBuilder builder = new StringBuilder(s);
        builder.deleteCharAt(index);
        return builder.toString();
    }

    public static <T> List<T> toList(T... array){
        List<T> list = new ArrayList<>();
        for(T t : array){
            list.add(t);
        }
        return list;
    }

    public static boolean isConnected(){
        try{
            final URL url = new URL("http://www.google.com");
            final URLConnection conn = url.openConnection();
            conn.connect();
            conn.getInputStream().close();
            return true;
        }catch(Exception ex){
            return false;
        }
    }

    public static void runDelayedTask(Runnable runnable, long milliseconds){
        try{
            TimeUnit.MILLISECONDS.sleep(milliseconds);
            runnable.run();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void runDelayedTask(Runnable runnable, long amount, TimeUnit timeUnit){
        try{
            timeUnit.sleep(amount);
            runnable.run();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public static String toString(int i){
        return Integer.toString(i);
    }

    public static int intValueOf(String s){
        return Integer.valueOf(s);
    }

    public static String toString(long l){
        return Long.toString(l);
    }

    public static long longValueOf(String s){
        return Long.valueOf(s);
    }

    public static String toString(double d){
        return Double.toString(d);
    }

    public static double doubleValueOf(String s){
        return Double.valueOf(s);
    }

    public static String toString(float f){
        return Float.toString(f);
    }

    public static float floatValueOf(String s){ return Float.valueOf(s); }

    public static <T> T[] addToArray(T[] originalArray, T objectToAdd){
        T[] r = Arrays.copyOf(originalArray, originalArray.length + 1);
        r[originalArray.length] = objectToAdd;
        return r;
    }

    public static long toMillis(int seconds){
        return seconds * 1000;
    }
}
