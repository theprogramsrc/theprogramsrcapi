package xyz.theprogramsrc.tpsapi.programmingUtils.connection;

import xyz.theprogramsrc.tpsapi.*;

import java.io.*;
import java.net.URL;

public abstract class TPSConnection extends TPS implements Connection{

    public TPSConnection(TheProgramSrcClass theProgramSrcClass){
        super(theProgramSrcClass);
    }

    public boolean isConnected(){
        try{
            if((new BufferedReader(new InputStreamReader(((new URL("https://raw.githubusercontent.com/TheProgramSrc/TheProgramSrcAPIPlugin/master/db-files/ConnectionTester")).openConnection()).getInputStream()))).readLine().equalsIgnoreCase("test passed!")){
                this.run(true);
                return true;
            }else{
                this.run(false);
                return false;
            }
        }catch (Exception ex){
            this.run(false);
            return false;
        }
    }
}
