package xyz.theprogramsrc.tpsapi.programmingUtils.connection;

public interface Connection{
    void run(boolean isConnected);
}
