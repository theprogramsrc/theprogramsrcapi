package xyz.theprogramsrc.tpsapi.programmingUtils.serialization;

import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.util.Map;

@SuppressWarnings ("ALL")
public class ItemSerializer{

    public static String serialize(ItemStack item){
        JSONObject result = new JSONObject();
        result.put("type", item != null ? item.getType().name() : "STONE");
        result.put("amount", item != null && item.getAmount() != 0 ? item.getAmount() : 1);
        result.put("damage", item != null ? item.getDurability() : 0);
        if(item != null){
            if(!item.getEnchantments().isEmpty()){ result.put("enchantments", item.getEnchantments()); }
            if(item.getItemMeta() != null){ result.put("itemMeta",item.getItemMeta().serialize()); }
        }
        return result.toJSONString();
    }

    public static String serialize(String path, ItemStack item){
        JSONObject result = new JSONObject();
        result.put(path+".type", item != null ? item.getType().name() : "STONE");
        result.put(path+".amount", item != null && item.getAmount() != 0 ? item.getAmount() : 1);
        result.put(path+".damage", item != null ? item.getDurability() : 0);
        if(item != null){
            if(!item.getEnchantments().isEmpty()){ result.put(path+".enchantments", item.getEnchantments()); }
            if(item.getItemMeta() != null){ result.put(path+".itemMeta",item.getItemMeta().serialize()); }
        }
        return result.toJSONString();
    }

    public static ItemStack deserialize(String serializedItem){
        JSONObject json = ((JSONObject) JSONValue.parse(serializedItem));
        ItemStack item = new ItemStack(Material.valueOf(((String)json.get("type"))), ((Integer)json.get("amount")));
        item.setDurability(((Short)json.get("damage")));
        Map enchants = null;
        ItemMeta meta = null;
        if(json.containsKey("enchantments")){ enchants = ((Map)json.get("enchantments")); }
        if(json.containsKey("itemMeta")){ meta = ((ItemMeta) ConfigurationSerialization.deserializeObject(json, ConfigurationSerialization.getClassByAlias("ItemMeta"))); }
        if(enchants != null && !enchants.isEmpty()){ item.addEnchantments(enchants); }
        if(meta != null){ item.setItemMeta(meta); }
        return item;
    }

    public static ItemStack deserialize(String path, String serializedItem){
        JSONObject json = ((JSONObject) JSONValue.parse(serializedItem));
        ItemStack item = new ItemStack(Material.valueOf(((String)json.get(path+".type"))), ((Integer)json.get(path+".amount")));
        item.setDurability(((Short)json.get(path+".damage")));
        Map enchants = null;
        ItemMeta meta = null;
        if(json.containsKey(path+".enchantments")){ enchants = ((Map)json.get(path+".enchantments")); }
        if(json.containsKey(path+".itemMeta")){ meta = ((ItemMeta) ConfigurationSerialization.deserializeObject(json, ConfigurationSerialization.getClassByAlias("ItemMeta"))); }
        if(enchants != null && !enchants.isEmpty()){ item.addEnchantments(enchants); }
        if(meta != null){ item.setItemMeta(meta); }
        return item;
    }
}
