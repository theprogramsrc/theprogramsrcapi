package xyz.theprogramsrc.tpsapi.programmingUtils.serialization;

import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.*;
import org.json.simple.*;
import java.util.UUID;

@SuppressWarnings ("ALL")
public class PlayerSerializer{

    public static String serialize(Player player){
        Utils.notNull(player,"Player cannot be null");
        JSONObject serializedPlayer = new JSONObject();
        serializedPlayer.put("uuid",player.getUniqueId());
        return serializedPlayer.toJSONString();
    }

    public static Player deserialize(String serializedPlayer){
        Utils.notNull(serializedPlayer,"SerializedPlayer cannot be null!");
        JSONObject player = ((JSONObject)JSONValue.parse(serializedPlayer));
        Player result = Bukkit.getPlayer(UUID.fromString(((String)player.get("uuid"))));
        if(result == null){ throw new NullPointerException("Result of player deserialization is null!"); }else{ return result; }
    }

}
