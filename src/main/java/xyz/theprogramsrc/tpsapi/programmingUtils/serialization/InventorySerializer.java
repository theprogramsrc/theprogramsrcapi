package xyz.theprogramsrc.tpsapi.programmingUtils.serialization;

import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;
import org.bukkit.*;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.*;
import org.json.simple.*;

import java.util.*;

@SuppressWarnings ("ALL")
public class InventorySerializer{

    public static String serialize(Inventory inventory){
        JSONObject result = new JSONObject();

        if(inventory.getType() == InventoryType.CHEST || inventory.getType() == InventoryType.ENDER_CHEST){
            result.put("size",inventory.getSize());
            result.put("title",inventory.getTitle());
            result.put("maxStackSize",inventory.getMaxStackSize());
            for(int i=0;i<inventory.getSize();++i){
                if(inventory.getItem(i) != null && inventory.getItem(i).getType() != Material.AIR){
                    result.put("item-"+i, ItemSerializer.serialize("itemStack-"+i,inventory.getItem(i)));
                }
            }
        }
        return result.toJSONString();
    }

    public static Inventory deserialize(String serialized){
        JSONObject jsonItem = ((JSONObject) JSONValue.parse(serialized));

        Integer size = ((Integer)jsonItem.get("size"));
        String title = ((String)jsonItem.get("title"));
        Integer maxStackSize = ((Integer)jsonItem.get("maxStackSize"));
        HashMap<Integer,ItemStack> items = new HashMap<>();
        for(int i=0;i<size;++i){
            ItemStack item = ItemSerializer.deserialize("itemStack-"+i, ((String)jsonItem.get("item-"+i)));
            items.put(i,item);
        }
        Inventory inv = Bukkit.createInventory(null,size, Utils.ct(title));
        inv.setMaxStackSize(maxStackSize);
        items.forEach(inv::setItem);
        return inv;
    }


}
