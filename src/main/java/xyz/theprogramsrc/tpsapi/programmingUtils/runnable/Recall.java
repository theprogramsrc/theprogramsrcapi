package xyz.theprogramsrc.tpsapi.programmingUtils.runnable;

public interface Recall<OBJ>{
    void run(OBJ var0);
}
