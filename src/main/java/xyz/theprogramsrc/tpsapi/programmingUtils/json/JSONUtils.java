package xyz.theprogramsrc.tpsapi.programmingUtils.json;

import org.json.simple.*;
import java.util.*;

@SuppressWarnings ("ALL")
public class JSONUtils{

    private JSONObject json;

    public JSONUtils(JSONObject jsonObject){ this.json = jsonObject; }

    public JSONUtils(String jsonString){ this.json = ((JSONObject)JSONValue.parse(jsonString)); }

    public JSONUtils set(String key, Object value){
        this.json.put(key,value);
        return this;
    }

    public Boolean getBoolean(String key){ return Boolean.parseBoolean(this.getString(key)); }

    public String getString(String key){ return ((String)this.json.get(key)); }

    public Integer getInteger(String key){ return Integer.parseInt(this.getString(key)); }

    public Object get(String key){ return this.json.get(key); }

    public String serialize(){ return this.json.toJSONString(); }

    public Set<String> getKeys(){
        Set<String> keys = new HashSet<>();
        for(Object o : this.getObject().keySet()){
            String key = ((String) o);
            keys.add(key);
        }
        return keys;
    }

    public JSONUtils getArray(String key){
        return new JSONUtils(((JSONObject)this.get(key)));
    }

    public HashMap<String, Object> getMap(){
        HashMap<String, Object> map = new HashMap<>();
        this.getKeys().forEach(s->map.put(s,this.get(s)));
        return map;
    }

    public void addAll(HashMap<String, ?> all){ all.forEach(this::set); }

    public JSONObject getObject(){ return this.json; }
}
