package xyz.theprogramsrc.tpsapi.gameManagement.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

@SuppressWarnings ({"unused", "RedundantSuppression"})
public class LocationUtils{

    public static String serialize(Location l){
        String x = Utils.toString(l.getX());
        String y = Utils.toString(l.getY());
        String z = Utils.toString(l.getZ());
        String yaw = Utils.toString(l.getYaw());
        String pitch = Utils.toString(l.getPitch());
        String world = l.getWorld().getName();
        return x + ":" + y + ":" + z + ":" + yaw + ":" + pitch + ":" + world;
    }

    public static Location deserialize(String s){
        String[] loc = s.split(":");
        double x = Utils.doubleValueOf(loc[0]);
        double y = Utils.doubleValueOf(loc[1]);
        double z = Utils.doubleValueOf(loc[2]);
        float yaw = Utils.floatValueOf(loc[3]);
        float pitch = Utils.floatValueOf(loc[4]);
        World world = Bukkit.getWorld(loc[5]);
        return new Location(world, x, y, z, yaw, pitch);
    }
}
