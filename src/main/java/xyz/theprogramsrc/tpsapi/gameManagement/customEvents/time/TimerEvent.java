package xyz.theprogramsrc.tpsapi.gameManagement.customEvents.time;

import org.bukkit.event.*;

public class TimerEvent extends Event{

    private static HandlerList handlerList = new HandlerList();

    private Time time;

    public TimerEvent(Time time){ this.time = time; }

    public Time getTime(){ return this.time; }

    public HandlerList getHandlers(){ return handlerList; }

    public static HandlerList getHandlerList(){ return handlerList; }
}
