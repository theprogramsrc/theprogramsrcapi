package xyz.theprogramsrc.tpsapi.gameManagement.customEvents;

import xyz.theprogramsrc.tpsapi.*;
import xyz.theprogramsrc.tpsapi.gameManagement.customEvents.player.DisplayNameChangeEvent;
import xyz.theprogramsrc.tpsapi.gameManagement.customEvents.time.*;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class EventsManager extends TPS{

    private static boolean timerEnabled = false;
    private HashMap<Player, String> names = new HashMap<>();

    public EventsManager(TheProgramSrcClass theProgramSrcClass){
        super(theProgramSrcClass);
        this.timerEvent();
        this.displayNameChangeEvent();
    }

    private void timerEvent(){
        if(!timerEnabled){
            timerEnabled = true;

            for(final Time t : Time.values()){
                Bukkit.getScheduler().scheduleSyncRepeatingTask(this.getPlugin(), ()-> EventsManager.this.callEvents(new TimerEvent(t)), 0L, t.getTime());
            }
        }
    }

    private void displayNameChangeEvent(){
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this.getPlugin(),()->{
            for(Player p : Bukkit.getOnlinePlayers()){
                if(!EventsManager.this.names.containsKey(p)){
                    EventsManager.this.names.put(p,p.getDisplayName());
                }else{
                    if(!EventsManager.this.names.get(p).equalsIgnoreCase(p.getDisplayName())){
                        DisplayNameChangeEvent e = new DisplayNameChangeEvent(p,EventsManager.this.names.get(p));
                        EventsManager.this.callEvents(e);
                        p.setDisplayName(Utils.ct(e.getDisplayName()));
                    }
                }
            }
        },0L,100L);
    }
}
