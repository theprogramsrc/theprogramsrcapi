package xyz.theprogramsrc.tpsapi.gameManagement.customEvents.player;

import xyz.theprogramsrc.tpsapi.gameManagement.dialog.Dialog;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerDialogCloseEvent extends PlayerEvent implements Cancellable{

    private static HandlerList handlerList = new HandlerList();

    private String title;
    private String subtitle;
    private String actionbar;
    private boolean cancelled = false;

    public PlayerDialogCloseEvent(Dialog dialog){
        super(dialog.getPlayer());
        this.title = dialog.getTitle();
        this.subtitle = dialog.getSubtitle();
        this.actionbar = dialog.getActionbar();
    }

    public String getTitle(){
        return title;
    }

    public String getSubtitle(){
        return subtitle;
    }

    public String getActionbar(){
        return actionbar;
    }

    @Override
    public boolean isCancelled(){ return cancelled; }

    @Override
    public void setCancelled(boolean cancelled){ this.cancelled = cancelled; }

    public static HandlerList getHandlerList(){ return handlerList; }

    @Override
    public HandlerList getHandlers(){ return handlerList; }
}
