package xyz.theprogramsrc.tpsapi.gameManagement.customEvents.player;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class DisplayNameChangeEvent extends PlayerEvent{

    private String displayName;
    private static HandlerList handlerList = new HandlerList();

    public DisplayNameChangeEvent(Player player, String displayName){
        super(player);
        this.displayName = displayName;
    }

    public String getDisplayName(){ return displayName; }

    public void setDisplayName(String displayName){ this.displayName = displayName; }

    @Override
    public HandlerList getHandlers(){ return handlerList; }

    public static HandlerList getHandlerList(){ return handlerList; }
}
