package xyz.theprogramsrc.tpsapi.gameManagement.customEvents.sky;

import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.player.PlayerEvent;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.skycolor.SkyColor;

public class PlayerSkyColorChangeEvent extends PlayerEvent implements Cancellable{

    private static HandlerList handlers = new HandlerList();
    private boolean cancelled = false;
    private SkyColor color;

    public PlayerSkyColorChangeEvent(Player player, SkyColor color){
        super(player);
        this.color = color;
    }

    public void setColor(SkyColor color){ this.color = color; }

    public SkyColor getSkyColor(){ return this.color; }

    @Override
    public HandlerList getHandlers(){ return handlers; }

    public static HandlerList getHandlerList(){ return handlers; }

    @Override
    public void setCancelled(boolean cancelled){ this.cancelled = cancelled; }

    @Override
    public boolean isCancelled(){ return this.cancelled; }
}
