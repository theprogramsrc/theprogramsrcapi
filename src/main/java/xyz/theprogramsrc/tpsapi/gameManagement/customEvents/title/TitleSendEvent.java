package xyz.theprogramsrc.tpsapi.gameManagement.customEvents.title;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class TitleSendEvent extends PlayerEvent{
    private static final HandlerList handlers = new HandlerList();
    private String title;
    private String subtitle;
    private boolean cancelled = false;

    public TitleSendEvent(Player player, String title, String subtitle) {
        super(player);
        this.title = title;
        this.subtitle = subtitle;
    }

    public HandlerList getHandlers() { return handlers; }

    public static HandlerList getHandlerList() { return handlers; }

    public String getTitle() { return this.title; }

    public void setTitle(String title) { this.title = title; }

    public String getSubtitle() { return this.subtitle; }

    public void setSubtitle(String subtitle) { this.subtitle = subtitle; }

    public boolean isCancelled() { return this.cancelled; }

    public void setCancelled(boolean cancelled) { this.cancelled = cancelled; }
}
