package xyz.theprogramsrc.tpsapi.gameManagement.customEvents.player;

import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import xyz.theprogramsrc.tpsapi.gameManagement.dialog.Dialog;

public class PlayerDialogOpenEvent extends PlayerEvent implements Cancellable{

    private static HandlerList handlerList = new HandlerList();
    private boolean cancelled = false;
    private String title;
    private String subtitle;
    private String actionbar;

    public PlayerDialogOpenEvent(Dialog dialog){
        super(dialog.getPlayer());
        this.title = dialog.getTitle();
        this.subtitle = dialog.getSubtitle();
        this.actionbar = dialog.getActionbar();
    }

    public String getTitle(){
        return title;
    }

    public String getSubtitle(){
        return subtitle;
    }

    public String getActionbar(){
        return actionbar;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setSubtitle(String subtitle){
        this.subtitle = subtitle;
    }

    public void setActionbar(String actionbar){
        this.actionbar = actionbar;
    }

    public static HandlerList getHandlerList(){ return handlerList; }

    @Override
    public HandlerList getHandlers(){ return handlerList; }

    @Override
    public boolean isCancelled(){ return this.cancelled; }

    @Override
    public void setCancelled(boolean cancelled){ this.cancelled = cancelled; }
}
