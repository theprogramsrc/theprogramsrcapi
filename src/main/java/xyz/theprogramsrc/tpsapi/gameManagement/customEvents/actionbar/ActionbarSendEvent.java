package xyz.theprogramsrc.tpsapi.gameManagement.customEvents.actionbar;

import org.bukkit.entity.Player;
import org.bukkit.event.*;

public class ActionbarSendEvent extends Event implements Cancellable{

    private static HandlerList handlers = new HandlerList();
    private boolean cancelled = false;

    private Player p;
    private String msg;

    public ActionbarSendEvent(Player player, String message){
        this.p = player;
        this.msg = message;
    }

    public Player getPlayer(){ return this.p; }

    public String getActionbarMessage(){ return this.msg; }

    public HandlerList getHandlers(){ return handlers; }

    public static HandlerList getHandlerList(){ return handlers; }

    public void setActionbarMessage(String actionbarMessage){ this.msg = actionbarMessage; }

    @Override
    public boolean isCancelled(){ return this.cancelled; }

    @Override
    public void setCancelled(boolean cancelled){ this.cancelled = cancelled; }
}
