package xyz.theprogramsrc.tpsapi.gameManagement.dialog;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.gameManagement.customEvents.player.PlayerDialogCloseEvent;
import xyz.theprogramsrc.tpsapi.gameManagement.customEvents.player.PlayerDialogOpenEvent;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.Actionbar;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.Title;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;
import xyz.theprogramsrc.tpsapi.programmingUtils.runnable.Recall;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@SuppressWarnings ("ALL")
public abstract class Dialog implements Listener{

    private TheProgramSrcClass tps;
    private Player p;
    private String title;
    private String subtitle;
    private String actionbar;
    private HashMap<String, String> placeholders = new HashMap<>();
    private Recall<Player> recall;

    public Dialog(TheProgramSrcClass theProgramSrcClass, Player player, String title, String subtitle, String actionbar){
        this.tps = theProgramSrcClass;
        this.p = player;
        this.title = title;
        this.subtitle = subtitle;
        this.actionbar = actionbar;
        this.openDialog();
    }

    public Dialog setRecall(Recall<Player> recall){
        this.recall = recall;
        return this;
    }

    public Dialog openDialog(){
        PlayerDialogOpenEvent event = new PlayerDialogOpenEvent(this);
        if(!event.isCancelled()){
            Title.sendTitle(this.getPlayer(), 0, 9999, 0, Utils.ct(this.apply(this.getTitle())), Utils.ct(this.apply(this.getSubtitle())));
            Actionbar.sendActionbar(this.getPlayer(), Utils.ct(this.apply(this.getActionbar())));
            this.tps.getPlugin().getServer().getScheduler().runTask(this.tps.getPlugin(), this.getPlayer()::closeInventory);
            this.tps.getPlugin().getServer().getPluginManager().registerEvents(this, this.tps.getPlugin());
        }
        return this;
    }

    public Dialog close(){
        PlayerDialogCloseEvent event = new PlayerDialogCloseEvent(this);
        if(!event.isCancelled()){
            Title.clearTitle(this.getPlayer());
            Actionbar.clearActionbar(this.getPlayer());
            if(this.recall != null) this.recall.run(this.getPlayer());
            HandlerList.unregisterAll(this);
        }
        return this;
    }

    public Player getPlayer(){
        return p;
    }

    public abstract boolean onResult(final String playerInput);

    @EventHandler(priority = EventPriority.LOW)
    public void onChat(AsyncPlayerChatEvent e){
        if(!e.getPlayer().equals(this.p)) return;
        e.setCancelled(true);
        String msg = e.getMessage();
        if(msg.equals("close") || msg.equals("cerrar")){
            Utils.sendMessage(this.p, this.tps.getLanguageManager().getPhrase("dialog-close-message"));
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        if(e.getPlayer().equals(this.getPlayer())){
            this.close();
        }
    }

    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent e){
        if(e.getPlayer().equals(this.getPlayer())){
            this.close();
        }
    }

    public Dialog addPlaceholder(String var0, String var1){
        this.placeholders.put(var0, var1);
        return this;
    }

    public Dialog addPlaceholders(HashMap<String, String> placeholders){
        this.placeholders.putAll(placeholders);
        return this;
    }

    public Dialog remPlaceholder(String var0){
        this.placeholders.remove(var0);
        return this;
    }

    public Dialog remPlaceholders(String[] keys){
        for(String s : keys){
            this.remPlaceholder(s);
        }
        return this;
    }

    private String apply(String text){
        String r = text;
        Map.Entry<String, String> e;

        for(Iterator<Map.Entry<String, String>> it = this.placeholders.entrySet().iterator(); it.hasNext(); r = r.replace(e.getKey(), e.getValue())){
            e = it.next();
        }

        return r;
    }

    public String getTitle(){
        return title;
    }

    public Dialog setTitle(String title){
        this.title = title;
        return this;
    }

    public Dialog setSubtitle(String subtitle){
        this.subtitle = subtitle;
        return this;
    }

    public String getSubtitle(){
        return subtitle;
    }

    public Dialog setActionbar(String actionbar){
        this.actionbar = actionbar;
        return this;
    }

    public String getActionbar(){
        return actionbar;
    }


}
