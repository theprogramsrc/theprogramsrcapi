package xyz.theprogramsrc.tpsapi.gameManagement.packets.packets;

import xyz.theprogramsrc.tpsapi.gameManagement.customEvents.actionbar.ActionbarSendEvent;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.PacketUtils;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.actionbar.*;

public class Actionbar{

    public static void sendActionbar(Player player, String message){
        if(message == null){
            message = "";
        }
        xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.actionbar.Actionbar actionbar;
        String v = PacketUtils.getVersion();
        if(v.contains("v1_8_R")){
            actionbar = new Actionbar_1_8();
        }else if(v.contains("v1_9_R")){
            actionbar = new Actionbar_1_9();
        }else if(v.contains("v1_10_R")){
            actionbar = new Actionbar_1_10();
        }else if(v.contains("v1_11_R")){
            actionbar = new Actionbar_1_11();
        }else if(v.contains("v1_12_R")){
            actionbar = new Actionbar_1_12();
        }else{
            actionbar = new Actionbar_1_13();
        }
        ActionbarSendEvent e = new ActionbarSendEvent(player, message);
        Bukkit.getPluginManager().callEvent(e);
        if(!e.isCancelled()){ actionbar.sendActionbar(e.getPlayer(), Utils.ct(e.getActionbarMessage())); }
    }

    public static void clearActionbar(Player player){
        sendActionbar(player,"");
    }
}
