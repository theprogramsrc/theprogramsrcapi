package xyz.theprogramsrc.tpsapi.gameManagement.packets.packets;

import xyz.theprogramsrc.tpsapi.gameManagement.customEvents.tablist.TablistSendEvent;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.PacketUtils;
import org.bukkit.*;
import org.bukkit.entity.Player;

import java.lang.reflect.*;

public class PlayerTablist{

    public static void sendTabTitle(Player player, String header, String footer) {
        if (header == null) {
            header = "";
        }

        header = ChatColor.translateAlternateColorCodes('&', header);
        if (footer == null) {
            footer = "";
        }
        footer = ChatColor.translateAlternateColorCodes('&', footer);


        TablistSendEvent e = new TablistSendEvent(player, header, footer);
        Bukkit.getPluginManager().callEvent(e);
        footer = e.getFooter();
        header = e.getHeader();

        if (!e.isCancelled()) {
            header = header.replace("{Player}", player.getDisplayName());
            footer = footer.replace("{Player}", player.getDisplayName());

            try {
                Object tabHeader = PacketUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + header + "\"}");
                Object tabFooter = PacketUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + footer + "\"}");
                Constructor<?> titleConstructor = PacketUtils.getNMSClass("PacketPlayOutPlayerListHeaderFooter").getConstructor();
                Object packet = titleConstructor.newInstance();

                Field aField;
                try {
                    aField = packet.getClass().getDeclaredField("a");
                    aField.setAccessible(true);
                    aField.set(packet, tabHeader);
                    aField = packet.getClass().getDeclaredField("b");
                    aField.setAccessible(true);
                    aField.set(packet, tabFooter);
                } catch (Exception ex) {
                    aField = packet.getClass().getDeclaredField("header");
                    aField.setAccessible(true);
                    aField.set(packet, tabHeader);
                    Field bField = packet.getClass().getDeclaredField("footer");
                    bField.setAccessible(true);
                    bField.set(packet, tabFooter);
                }

                PacketUtils.sendPacket(player, packet);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
    }
}
