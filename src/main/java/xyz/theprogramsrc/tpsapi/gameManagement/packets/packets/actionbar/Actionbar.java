package xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.actionbar;

import org.bukkit.entity.Player;

public interface Actionbar{

    void sendActionbar(Player player, String message);
}
