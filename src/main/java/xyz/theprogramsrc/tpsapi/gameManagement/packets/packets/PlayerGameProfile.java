package xyz.theprogramsrc.tpsapi.gameManagement.packets.packets;

import com.mojang.authlib.GameProfile;
import org.bukkit.entity.Player;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.PacketUtils;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

public class PlayerGameProfile{

    public static GameProfile get(Player p){
        try{
            Class<?> craftPlayer = PacketUtils.getOCBClass("entity.CraftPlayer");
            return ((GameProfile) Utils.requireNonNull(PacketUtils.getMethod(craftPlayer, "getProfile")).invoke(p));
        }catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
}
