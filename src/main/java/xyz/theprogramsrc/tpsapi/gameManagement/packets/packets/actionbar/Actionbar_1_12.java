package xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.actionbar;

import xyz.theprogramsrc.tpsapi.gameManagement.packets.PacketUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Actionbar_1_12 implements Actionbar{

    @Override
    public void sendActionbar(Player player, String message){
        try{
            message = message.replace("{Player}", player.getName());
            Object icbc = PacketUtils.getNMSClass("ChatComponentText").getConstructor(String.class).newInstance(ChatColor.translateAlternateColorCodes('&', message));
            Object ppoc = PacketUtils.getNMSClass("ChatMessageType").getField("GAME_INFO").get(null);
            Object nmsp = PacketUtils.getNMSClass("PacketPlayOutChat").getConstructor(PacketUtils.getNMSClass("IChatBaseComponent"), PacketUtils.getNMSClass("ChatMessageType")).newInstance(icbc, ppoc);
            PacketUtils.sendPacket(player,nmsp);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
