package xyz.theprogramsrc.tpsapi.gameManagement.packets.packets;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.bukkit.entity.Player;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.logging.Level;

@SuppressWarnings("ALL")
public class SkinTexture {
    private String url;
    private long createdAt;

    public static SkinTexture fromPlayer(Player p) {
        GameProfile profile = PlayerGameProfile.get(p);

        try {
            Property property = (Property)profile.getProperties().get("textures").iterator().next();
            String texture = property.getValue();
            String var4 = base64ToUrl(texture);
            return var4 != null && !var4.equals("null") ? new SkinTexture(var4, System.currentTimeMillis()) : null;
        } catch (NoSuchElementException var5) {
            return null;
        }
    }

    public static SkinTexture fromMojang(String playerName){
        try{
            String url = String.format("https://api.mojang.com/users/profiles/minecraft/%s", playerName);
            String response = IOUtils.toString(new URL(url).openStream(), Charset.forName("UTF-8"));
            String uuid = ((new JsonParser()).parse(response).getAsJsonObject().get("id").getAsString());
            uuid = (new StringBuffer(uuid)).insert(8, "-").insert(13, "-").insert(18, "-").insert(23, "-").toString();
            return fromMojang(UUID.fromString(uuid));
        }catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public static SkinTexture fromMojang(UUID uuid){
        try{
            String url = String.format("https://sessionserver.mojang.com/session/minecraft/profile/%s?unsigned=false", uuid.toString().replace("-",""));
            String response = IOUtils.toString(new URL(url).openStream(), Charset.forName("UTF-8"));
            String base64 = ((new JsonParser()).parse(response).getAsJsonObject().get("properties").getAsJsonArray().get(0).getAsJsonObject()).get("value").getAsString();
            return new SkinTexture(base64ToUrl(base64), System.currentTimeMillis());
        }catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    private static String base64ToUrl(String base64) {
        try {
            String decodedJson = new String(Base64.decodeBase64(base64.getBytes()));
            JsonObject skin = ((JsonObject)(new JsonParser()).parse(decodedJson)).getAsJsonObject("textures").getAsJsonObject("SKIN");
            if (skin != null){
                return skin.get("url").getAsString();
            }
        } catch (Exception ex) {
            Utils.Log(Level.SEVERE, "Could not retrieve Url from Base64:");
            Utils.Log(Level.INFO, base64);
            Utils.Log(Level.SEVERE, "Error");
            ex.printStackTrace();
        }
        return null;
    }

    public SkinTexture(String url, long createdAt) {
        this.url = url;
        this.createdAt = createdAt;
    }

    public SkinTexture(String url) {
        this.url = url.split(":split:")[0];
        this.createdAt = Long.valueOf(url.split(":split:")[1]);
    }

    public String getUrl() {
        return this.url;
    }

    public long getCreatedAt() {
        return this.createdAt;
    }

    public String toString() {
        return this.url + ":split:" + this.createdAt;
    }
}
