package xyz.theprogramsrc.tpsapi.gameManagement.packets.packets;

import xyz.theprogramsrc.tpsapi.gameManagement.customEvents.sky.PlayerSkyColorChangeEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.skycolor.SkyColor;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.skycolor.SkyColorChange;

@SuppressWarnings ("unused")
public class Sky{

    public static void changeSky(Player player, SkyColor color){
        PlayerSkyColorChangeEvent e = new PlayerSkyColorChangeEvent(player,color);
        Bukkit.getPluginManager().callEvent(e);
        SkyColorChange c = new SkyColorChange();
        c.changeSky(e.getPlayer(),e.getSkyColor());
    }
}
