package xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.actionbar;

import xyz.theprogramsrc.tpsapi.gameManagement.packets.PacketUtils;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;

public class Actionbar_1_13 implements Actionbar{

    private static Class<?> IChatBaseComponent = PacketUtils.getNMSClass("IChatBaseComponent");
    private static Class<?> ChatComponentText = PacketUtils.getNMSClass("ChatComponentText");
    private static Class<?> ChatMessageType = PacketUtils.getNMSClass("ChatMessageType");
    private static Class<?> PacketPlayOutChat = PacketUtils.getNMSClass("PacketPlayOutChat");

    @Override
    public void sendActionbar(Player player, String message){
        try{
            message = message.replace("{Player}", player.getName());
            Object ab = ChatComponentText.getConstructor(String.class).newInstance(message);
            Constructor<?> ac = PacketPlayOutChat.getConstructor(IChatBaseComponent, ChatMessageType);
            Object packet = ac.newInstance(ab, ChatMessageType.getMethod("a", Byte.TYPE).invoke(null, (byte)2));
            PacketUtils.sendPacket(player,packet);
        }catch (Exception e){ e.printStackTrace(); }
    }
}
