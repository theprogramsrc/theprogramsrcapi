package xyz.theprogramsrc.tpsapi.gameManagement.packets.packets;

import org.bukkit.*;
import org.bukkit.entity.Player;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.PacketUtils;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

import java.lang.reflect.*;

public class DemoMessage{

    public static void sendDemoMessage(OfflinePlayer player){
        if(player.isOnline()){
            try{
                Player p = Bukkit.getOnlineMode() ? Bukkit.getPlayer(player.getUniqueId()) : Bukkit.getPlayer(player.getName());
                Class<?> gameStateChange = PacketUtils.getNMSClass("PacketPlayOutGameStateChange");
                Constructor<?> playOutConstructor = PacketUtils.getConstructor(gameStateChange, Integer.TYPE, Float.TYPE);
                Object packet = Utils.requireNonNull(playOutConstructor).newInstance(5,0);
                PacketUtils.sendPacket(packet, p);
            }catch(Exception ex){
                ex.printStackTrace();
            }

        }
    }
}
