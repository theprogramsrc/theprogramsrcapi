package xyz.theprogramsrc.tpsapi.gameManagement.packets;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class PacketUtils{

    public static String getVersion() {
        return Bukkit.getServer().getClass().getPackage().getName().substring(23);
    }

    public static void sendPacket(Player player, Object packet) {
        try {
            Object handle = Utils.requireNonNull(getMethod(player.getClass(), "getHandle")).invoke(player);
            Object playerConnection = Utils.requireNonNull(getField(handle.getClass(), "playerConnection")).get(handle) ;
            Method m = getMethod(playerConnection.getClass(),"sendPacket", getNMSClass("Packet"));
            if(m != null){ m.invoke(playerConnection, packet); }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void sendPacket(Object packet, Player... players){
        for(Player p : players){
            sendPacket(p,packet);
        }
    }

    public static void sendPacket(ArrayList<Player> players, Object packet){
        Player[] p = new Player[players.size()];
        p = players.toArray(p);
        sendPacket(packet, p);
    }


    public static Class<?> getNMSClass(String localPackage){
        return getClass("net.minecraft.server."+getVersion()+"."+localPackage);
    }

    public static Class<?> getOCBClass(String localPackage){
        return getClass("org.bukkit.craftbukkit."+getVersion()+"."+localPackage);
    }

    public static Class<?> getClass(String name){
        try{
            return Class.forName(name);
        }catch(ClassNotFoundException e){
            e.printStackTrace();
            return null;
        }
    }

    public static Constructor<?> getConstructor(Class<?> clazz, Class<?>... params){
        try{
            return clazz.getConstructor(params);
        }catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public static Method getMethod(Class<?> clazz, String methodName, Class<?>... params) {
        try {
            return clazz.getMethod(methodName, params);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static Method getMethod(Class<?> clazz, String methodName){
        try {
            return clazz.getMethod(methodName);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static Field getField(Class<?> clazz, String fieldName){
        try{
            return clazz.getField(fieldName);
        }catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
}
