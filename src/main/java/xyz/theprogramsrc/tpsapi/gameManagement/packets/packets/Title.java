package xyz.theprogramsrc.tpsapi.gameManagement.packets.packets;

import xyz.theprogramsrc.tpsapi.gameManagement.customEvents.title.TitleSendEvent;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.*;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;
import org.bukkit.*;
import org.bukkit.entity.Player;

import java.lang.reflect.*;

public class Title{

    public static void sendTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle) {
        TitleSendEvent e = new TitleSendEvent(player, title, subtitle);
        Bukkit.getPluginManager().callEvent(e);
        String su = e.getSubtitle();
        String ti = e.getTitle();
        if (!e.isCancelled()) {
            try {
                Object ppot;
                Object icbc;
                Constructor pot;
                Object packet;
                if (ti != null) {
                    ti = Utils.ct(ti);
                    ppot = PacketUtils.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TIMES").get(null);
                    icbc = PacketUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + ti + "\"}");
                    pot = PacketUtils.getNMSClass("PacketPlayOutTitle").getConstructor(PacketUtils.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], PacketUtils.getNMSClass("IChatBaseComponent"), Integer.TYPE, Integer.TYPE, Integer.TYPE);
                    packet = pot.newInstance(ppot, icbc, fadeIn, stay, fadeOut);
                    PacketUtils.sendPacket(e.getPlayer(), packet);
                    ppot = PacketUtils.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TITLE").get(null);
                    icbc = PacketUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + ti + "\"}");
                    pot = PacketUtils.getNMSClass("PacketPlayOutTitle").getConstructor(PacketUtils.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], PacketUtils.getNMSClass("IChatBaseComponent"));
                    packet = pot.newInstance(ppot, icbc);
                    PacketUtils.sendPacket(e.getPlayer(), packet);
                }

                if (su != null) {
                    su = Utils.ct(su);
                    ppot = PacketUtils.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TIMES").get(null);
                    icbc = PacketUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + ti + "\"}");
                    pot = PacketUtils.getNMSClass("PacketPlayOutTitle").getConstructor(PacketUtils.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], PacketUtils.getNMSClass("IChatBaseComponent"), Integer.TYPE, Integer.TYPE, Integer.TYPE);
                    packet = pot.newInstance(ppot, icbc, fadeIn, stay, fadeOut);
                    PacketUtils.sendPacket(e.getPlayer(), packet);
                    ppot = PacketUtils.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("SUBTITLE").get(null);
                    icbc = PacketUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + su + "\"}");
                    pot = PacketUtils.getNMSClass("PacketPlayOutTitle").getConstructor(PacketUtils.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], PacketUtils.getNMSClass("IChatBaseComponent"), Integer.TYPE, Integer.TYPE, Integer.TYPE);
                    packet = pot.newInstance(ppot, icbc, fadeIn, stay, fadeOut);
                    PacketUtils.sendPacket(e.getPlayer(), packet);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void clearTitle(Player player) {
        sendTitle(player, 0, 0, 0, "", "");
    }


}
