package xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.skycolor;

import xyz.theprogramsrc.tpsapi.gameManagement.packets.PacketUtils;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.lang.reflect.*;

public class SkyColorChange{

    public void changeSky(Player player, SkyColor color){
        if(color == SkyColor.FREEZE){
            this.freeze(player);
        }else if(color == SkyColor.UNFREEZE){
            this.unfreeze(player);
        }else{
            this.change(player,color.getValue());
        }
    }

    private void change(Player player, int number){
        try{
            Class<?> packetClass = PacketUtils.getNMSClass("PacketPlayOutGameStateChange");
            Constructor<?> packetConstructor = packetClass.getConstructor(Integer.TYPE, Float.TYPE);
            Object packet = packetConstructor.newInstance(7, number);
            PacketUtils.sendPacket(player,packet);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    private void freeze(Player player){
        try{
            World w = player.getWorld();
            Class<?> packetClass = PacketUtils.getNMSClass("PacketPlayOutRespawn");
            Class<?> diffClass = PacketUtils.getNMSClass("EnumDifficulty");
            Class<?> wtClass = PacketUtils.getNMSClass("WorldType");
            Class<?> gameModeClass = PacketUtils.getNMSClass("EnumGamemode");
            Method diffGetById = PacketUtils.getMethod(diffClass, "getById", Integer.TYPE);
            Method gmGetById = PacketUtils.getMethod(gameModeClass, "getById", Integer.TYPE);
            Constructor<?> packetConstructor;
            Object packet;
            try {
                packetConstructor = packetClass.getConstructor(Integer.TYPE, diffClass, wtClass, gameModeClass);
                packet = packetConstructor.newInstance(w.getEnvironment().getId(), diffGetById.invoke((Object)null, w.getDifficulty().getValue()), wtClass.getField("NORMAL").get((Object)null), gmGetById.invoke((Object)null, player.getGameMode().getValue()));
            } catch (Exception ex) {
                Class<?> worldSettings = PacketUtils.getNMSClass("WorldSettings");
                Class<?>[] innerClasses = worldSettings.getDeclaredClasses();
                Class<?> wsGameMode = null;
                Class[] enumGamemodes = innerClasses;

                for(Class<?> enumGamemode : enumGamemodes){
                    if (enumGamemode.getSimpleName().equals("EnumGamemode")) {
                        wsGameMode = enumGamemode;
                    }
                }

                Method a = PacketUtils.getMethod(worldSettings, "a", Integer.TYPE);
                packetConstructor = packetClass.getConstructor(Integer.TYPE, diffClass, wtClass, wsGameMode);
                packet = packetConstructor.newInstance(w.getEnvironment().getId(), diffGetById.invoke(null, w.getDifficulty().getValue()), wtClass.getField("NORMAL").get(null), a.invoke(null, player.getGameMode().getValue()));
            }
            PacketUtils.sendPacket(player,packet);
            player.updateInventory();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    private void unfreeze(Player player){ player.teleport(player.getLocation()); }
}
