package xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.actionbar;

import xyz.theprogramsrc.tpsapi.gameManagement.packets.PacketUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Actionbar_1_11 implements Actionbar{

    @Override
    public void sendActionbar(Player player, String message){
        try{
            message = message.replace("{Player}", player.getName());
            Object icbc = PacketUtils.getNMSClass("ChatComponentText").getConstructor(String.class).newInstance(ChatColor.translateAlternateColorCodes('&', message));
            Object ppoc = PacketUtils.getNMSClass("PacketPlayOutChat").getConstructor(PacketUtils.getNMSClass("IChatBaseComponent"), Byte.TYPE).newInstance(icbc, (byte)2);
            PacketUtils.sendPacket(player,ppoc);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
