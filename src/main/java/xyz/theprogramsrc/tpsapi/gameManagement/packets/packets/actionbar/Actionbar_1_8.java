package xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.actionbar;

import xyz.theprogramsrc.tpsapi.gameManagement.packets.PacketUtils;
import org.bukkit.entity.Player;

public class Actionbar_1_8 implements Actionbar{

    @Override
    public void sendActionbar(Player player, String message){
        try{
            message = message.replace("{Player}", player.getName());
            Object icbc = PacketUtils.getNMSClass("IChatBaseComponent$ChatSerializer").getMethod("a", String.class).invoke(null, "{'text': '" + message + "'}");
            Object ppoc = PacketUtils.getNMSClass("PacketPlayOutChat").getConstructor(PacketUtils.getNMSClass("IChatBaseComponent"), Byte.TYPE).newInstance(icbc, (byte)2);
            PacketUtils.sendPacket(player,ppoc);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
