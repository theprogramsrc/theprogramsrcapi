package xyz.theprogramsrc.tpsapi.gameManagement.ui;

import org.bukkit.entity.Player;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.gameManagement.dialog.Dialog;
import xyz.theprogramsrc.tpsapi.programmingUtils.runnable.Recall;

@SuppressWarnings ("ALL")
public abstract class BrowserUI<T> extends UI{

    private String search = null;
    private int page = 1;
    private Recall<ActionEvent> back;

    public BrowserUI(TheProgramSrcClass theProgramSrcClass, Player player){
        super(theProgramSrcClass, player);
    }

    @Override
    public int getSize(){
        return 54;
    }

    @Override
    public void onUILoad(){
        this.clearItems();
        if(this.page == 0) this.page = 1;
        int x = this.page * 45 - 45;
        for(int i = 0; i < 45 && i < this.getObjects().length; ++i){
            T t = this.getObjects()[x];
            BrowserUIButton button = this.getItem(t);
            if(button != null && button.getItem() != null){
                this.removeItem(i);
                this.setItem(new UIButton(i, button.getItem(), button.getAction()));
            }
            x++;
        }
        this.removeItems(45, 46, 47, 48, 49, 50, 51, 52, 53);
        if(this.back != null){
            this.removeItem(45);
            this.setItem(new UIButton(45, this.getPreloadedItems().getBackItem(), this.back::run));
        } if(this.page != 1){
            this.removeItem(52);
            this.setItem(new UIButton(52, this.getPreloadedItems().getPrevItem(), e-> this.prevPage()));
        }
        if(this.page != ((this.getObjects().length / 45) + (this.getObjects().length % 45 != 0 ? 1 : 0))){
            this.removeItem(53);
            this.setItem(new UIButton(53, this.getPreloadedItems().getNextItem(), e-> this.nextPage()));
        }

        if(this.isSearching()){
            this.removeItem(49);
            this.setItem(new UIButton(49, this.getPreloadedItems().getEndSearchItem(), e->{
                this.endSearch();
                this.present();
            }));
        }else{
            this.removeItem(49);
            this.setItem(new UIButton(49, this.getPreloadedItems().getSearchItem(), e->new Dialog(getTPS(), e.getPlayer(), getLanguageManager().getPhrase("browserui-search-title"), getLanguageManager().getPhrase("browserui-search-subtitle"), getLanguageManager().getPhrase("browserui-search-actionbar")){
                @Override
                public boolean onResult(String playerInput){
                    BrowserUI.this.search(playerInput);
                    BrowserUI.this.present();
                    return true;
                }
            }));
        }
        this.open();
    }

    public void prevPage(){
        this.page--;
        this.onUILoad();
    }

    public void nextPage(){
        this.page++;
        this.onUILoad();
    }

    public void page(int page){
        this.page = page;
        this.onUILoad();
    }

    public void setBack(Recall<ActionEvent> back){
        this.back = back;
        this.present();
    }

    public void search(String search){
        this.search = search;
        this.page = 1;
        this.onUILoad();
    }

    public boolean isSearching(){
        return this.search != null;
    }

    public void endSearch(){
        this.search = null;
        this.page = 1;
        this.onUILoad();
    }

    public abstract T[] getObjects();

    public abstract BrowserUIButton getItem(T o);
}