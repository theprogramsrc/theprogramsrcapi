package xyz.theprogramsrc.tpsapi.gameManagement.ui;

import org.bukkit.event.inventory.InventoryAction;

public enum ClickType{
    RIGHT_CLICK(InventoryAction.PICKUP_HALF),
    MIDDLE_CLICK(InventoryAction.CLONE_STACK),
    LEFT_CLICK(InventoryAction.PICKUP_ALL),
    Q(InventoryAction.DROP_ONE_CURSOR),
    CTRL_Q(InventoryAction.DROP_ALL_CURSOR),
    SHIFT_CLICK(InventoryAction.MOVE_TO_OTHER_INVENTORY);


    private InventoryAction action;
    ClickType(InventoryAction inventoryAction){ this.action = inventoryAction; }

    public static ClickType fromInventoryAction(InventoryAction inventoryAction){
        for(ClickType clickType : values()){
            if(clickType.getInventoryAction() == inventoryAction){
                return clickType;
            }
        }
        return LEFT_CLICK;
    }

    public InventoryAction getInventoryAction(){ return action; }
}

