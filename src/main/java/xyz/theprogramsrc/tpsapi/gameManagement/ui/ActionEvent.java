package xyz.theprogramsrc.tpsapi.gameManagement.ui;

import org.bukkit.entity.Player;

@SuppressWarnings("ALL")
public class ActionEvent{

    private Player player;
    private ClickType clickType;

    public ActionEvent(Player player, ClickType clickType){
        this.player = player;
        this.clickType = clickType;
    }

    public Player getPlayer(){
        return player;
    }

    public ClickType getClickType(){
        return clickType;
    }
}
