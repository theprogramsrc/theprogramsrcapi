package xyz.theprogramsrc.tpsapi.gameManagement.ui;


public interface Action{
    void run(ActionEvent e);
}
