package xyz.theprogramsrc.tpsapi.gameManagement.ui;

import org.bukkit.inventory.ItemStack;
@SuppressWarnings("ALL")
public class UIButton{

    private Action action;
    private ItemStack item;
    private int slot;

    public UIButton(int slot, ItemStack itemStack, Action action){
        this.action = action;
        this.item = itemStack;
        this.slot = slot;
    }

    public Action getAction(){
        return action;
    }

    public ItemStack getItem(){
        return item;
    }

    public int getSlot(){
        return slot;
    }
}
