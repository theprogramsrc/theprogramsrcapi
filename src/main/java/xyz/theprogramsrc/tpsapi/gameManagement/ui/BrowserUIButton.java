package xyz.theprogramsrc.tpsapi.gameManagement.ui;

import org.bukkit.inventory.ItemStack;

@SuppressWarnings ("ALL")
public class BrowserUIButton{

    private Action action;
    private ItemStack item;

    public BrowserUIButton(ItemStack item, Action action){
        this.item = item;
        this.action = action;
    }

    public Action getAction(){
        return action;
    }

    public ItemStack getItem(){
        return item;
    }
}