package xyz.theprogramsrc.tpsapi.gameManagement.ui;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import xyz.theprogramsrc.tpsapi.TPS;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.gameManagement.items.ItemBuilder;
import xyz.theprogramsrc.tpsapi.gameManagement.items.XMaterial;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

import java.util.HashMap;

@SuppressWarnings("ALL")
public abstract class UI extends TPS implements Provider, Listener{

    private Player p;
    private TheProgramSrcClass tps;
    private HashMap<Integer, UIButton> actions = new HashMap<>();
    private Inventory inv;

    public UI(TheProgramSrcClass theProgramSrcClass, Player p){
        this(theProgramSrcClass,p,true);
    }

    public UI(TheProgramSrcClass theProgramSrcClass, Player p, boolean present){
        super(theProgramSrcClass, false);
        this.tps = theProgramSrcClass;
        this.p = p;
        if(present) this.present();
    }

    /**
     * This will run onUILoad() and open the UI
     */
    public void present(){
        HandlerList.unregisterAll(this);
        this.inv = this.tps.getPlugin().getServer().createInventory(null, this.getSize() != 0 ? this.getSize() : 54, Utils.ct(this.getTitle()));
        Bukkit.getPluginManager().registerEvents(this, tps.getPlugin());
        this.onUILoad();
        this.open();
    }


    /**
     * This will open the UI
     */
    public void open(){
        this.getPlayer().openInventory(this.getInventory());
    }

    public void close(){
        this.getPlayer().closeInventory();
        HandlerList.unregisterAll(this);
    }

    public Inventory getInventory(){
        return inv;
    }

    public Action getAction(int slot){
        return this.actions.get(slot).getAction();
    }

    public Player getPlayer(){
        return p;
    }

    public void setItem(UIButton uiButton){
        if(uiButton.getItem() != null){
            if(uiButton.getItem().getType() != XMaterial.AIR.parseMaterial()){
                this.inv.setItem(uiButton.getSlot(), uiButton.getItem());
                this.actions.put(uiButton.getSlot(), uiButton);
            }
        }
    }

    public void removeItem(int slot){
        this.inv.setItem(slot, XMaterial.AIR.parseItem());
        this.actions.remove(slot);
    }

    public void removeItems(int... slots){
        for(int slot : slots){
            this.removeItem(slot);
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent e){
        if(this.inv != null && e.getClickedInventory() != null && this.getTitle() != null && e.getClickedInventory().getTitle() != null){
            if(Utils.ct(this.getTitle()).equals(Utils.ct(e.getClickedInventory().getTitle()))){
                if(e.getCurrentItem() != null){
                    if(e.getCurrentItem().getType() != XMaterial.AIR.parseMaterial()){
                        e.setCancelled(true);
                        int slot = e.getSlot();
                        if(this.getAction(slot) != null){
                            ActionEvent event = new ActionEvent(this.getPlayer(), ClickType.fromInventoryAction(e.getAction()));
                            this.getAction(slot).run(event);
                            return;
                        }
                    }
                }
            }
        }
    }

    public void clearItems(){
        this.actions.clear();
        for(int i = 0; i < this.getSize(); ++i){
            this.inv.setItem(i, new ItemBuilder(XMaterial.AIR).construct());
        }
        this.getPlayer().updateInventory();
    }

    public abstract int getSize();

    public abstract String getTitle();
}

