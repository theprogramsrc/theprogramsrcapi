package xyz.theprogramsrc.tpsapi.gameManagement.user;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.gameManagement.customEvents.time.Time;
import xyz.theprogramsrc.tpsapi.gameManagement.customEvents.time.TimerEvent;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.SkinTexture;
import xyz.theprogramsrc.tpsapi.generalManagement.files.CustomFile.CustomFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

@SuppressWarnings ("ALL")
public class UserManager extends CustomFile{

    public UserManager(TheProgramSrcClass theProgramSrcClass){
        super(theProgramSrcClass, false, true);
    }

    @Override
    public String getFileName(){
        return "Users";
    }

    @Override
    public String getFileExtension(){
        return ".yml";
    }

    @Override
    public File getFileFolder(){
        return this.getDataFolder();
    }

    public void saveUser(User u){
        String p = u.getUuid().toString();
        this.set(p+".Name", u.getName());
        this.set(p+".Admin", u.isAdmin());
        this.set(p+".Skin", u.getSkin().toString());
        u.getExtraConfig().forEach((k,v)->{
            this.set(p+".ExtraConfig."+k, v);
        });
        this.save();
    }

    public void delUser(User u){
        this.remove(u.getUuid().toString());
        this.save();
    }

    public User getUser(UUID uuid){
        String p = uuid.toString();
        String name = this.getString(p+".Name");
        boolean admin = this.getBoolean(p+".Admin");
        SkinTexture skinTexture = !this.getString(p+".Skin").contains("null:split:") ? new SkinTexture(this.getString(p+".Skin")) : SkinTexture.fromMojang("Steve");
        HashMap<String, Object> extraConfig = new HashMap<>();
        if(this.contains(p+".ExtraConfig")){
            for(String s : this.getConfigurationSection(p+".ExtraConfig").getKeys(false)){
                extraConfig.put(s, this.get(p+".ExtraConfig."+s));
            }
        }
        User u = new User(getTPS(), uuid, name, skinTexture, admin);
        u.addExtraConfig(extraConfig);
        return u;
    }

    public User[] getUsers(){
        ArrayList<User> userArrayList = new ArrayList<>();
        for(String s : this.getKeys(false)){
            UUID uuid = UUID.fromString(s);
            userArrayList.add(this.getUser(uuid));
        }
        User[] users = new User[userArrayList.size()];
        users = userArrayList.toArray(users);
        return users;
    }

    public boolean containsUser(UUID uuid){
        return this.contains(uuid.toString());
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        Player p = e.getPlayer();
        if(p != null){
            if(!this.containsUser(p.getUniqueId())){
                SkinTexture skin = getTPS().getSkin(p);
                if(skin == null){
                    skin = SkinTexture.fromMojang("Steve");
                }
                User u = new User(getTPS(), p.getUniqueId(), p.getName(), skin, p.isOp());
                u.save();
            }
        }
    }

    @EventHandler
    public void sync(TimerEvent e){
        if(e.getTime() == Time.TICK){
            for(User u : this.getUsers()){
                if(u != null){
                    if(u.isOnline()){
                        if(u.getSkin() == null) u.setSkin(getSkin(u.getPlayer()) != null ? getSkin(u.getPlayer()) : SkinTexture.fromMojang("Steve"));
                    }
                }
            }
        }
    }
}
