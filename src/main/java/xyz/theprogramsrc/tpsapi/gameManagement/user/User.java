package xyz.theprogramsrc.tpsapi.gameManagement.user;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.SkinTexture;

import java.util.HashMap;
import java.util.UUID;

@SuppressWarnings ("ALL")
public class User{

    private String name;
    private UUID uuid;
    private SkinTexture skin;
    private boolean admin;
    private HashMap<String, Object> extraConfig;
    private TheProgramSrcClass tps;

    public User(TheProgramSrcClass theProgramSrcClass, UUID uuid, String name, SkinTexture skin, boolean admin){
        this.tps = theProgramSrcClass;
        this.uuid = uuid;
        this.name = name;
        this.skin = skin;
        this.admin = admin;
        this.extraConfig = new HashMap<>();
    }

    public String getName(){
        return name;
    }

    public UUID getUuid(){
        return uuid;
    }

    public SkinTexture getSkin(){
        return skin;
    }

    public boolean isAdmin(){
        return admin;
    }

    public User setAdmin(boolean admin){
        this.admin = admin;
        return this;
    }

    public HashMap<String, Object> getExtraConfig(){
        return extraConfig;
    }

    public User setSkin(SkinTexture skin){
        this.skin = skin;
        return this;
    }

    public User addExtraConfig(String path, Object value){
        this.extraConfig.put(path,value);
        return this;
    }

    public User addExtraConfig(HashMap<String,Object> extraConfig){
        this.extraConfig.putAll(extraConfig);
        return this;
    }

    public String getStringFromExtraConfig(String path){
        return ((String)this.getExtraConfig().get(path));
    }

    public int getIntFromExtraConfig(String path){
        return Integer.parseInt(this.getStringFromExtraConfig(path));
    }

    public boolean getBooleanFromExtraConfig(String path){
        return Boolean.parseBoolean(this.getStringFromExtraConfig(path));
    }

    public User remExtraConfig(String path){
        this.extraConfig.remove(path);
        return this;
    }

    public User remExtraConfig(HashMap<String,Object> extraConfig){
        extraConfig.forEach(this.extraConfig::remove);
        return this;
    }

    public User save(){
        this.tps.getUserManager().saveUser(this);
        return this;
    }

    public void delete(){
        this.tps.getUserManager().delUser(this);
    }

    public User clone(){
        User u = new User(this.tps, this.getUuid(), this.getName(), this.getSkin(), this.isAdmin());
        u.addExtraConfig(this.getExtraConfig());
        return u;
    }

    public boolean isOnline(){
        return this.getPlayer().isOnline();
    }

    public Player getPlayer(){
        return this.tps.getPlugin().getServer().getOnlineMode() ? Bukkit.getPlayer(this.getUuid()) : Bukkit.getPlayer(this.getName());
    }

    public boolean hasPermission(String permission){
        return this.isAdmin() || this.getPlayer().hasPermission(permission);
    }
}
