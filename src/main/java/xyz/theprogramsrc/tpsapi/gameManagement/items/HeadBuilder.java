package xyz.theprogramsrc.tpsapi.gameManagement.items;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.SkinTexture;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

import java.lang.reflect.Field;
import java.util.UUID;

@SuppressWarnings("ALL")
public class HeadBuilder{

    private ItemStack itemStack;

    public HeadBuilder(XMaterial headType, String headOwner){
        this.itemStack = headType.parseItem();
        SkullMeta meta = ((SkullMeta)this.itemStack.getItemMeta());
        this.itemStack.setItemMeta(meta);
        this.setOwner(headOwner);
    }

    public HeadBuilder setOwner(String owner){
        if(this.itemStack.getItemMeta() != null && this.itemStack.getItemMeta() instanceof SkullMeta){
            SkullMeta meta = ((SkullMeta)this.itemStack.getItemMeta());
            meta.setOwner(owner);
            this.itemStack.setItemMeta(meta);
        }
        return this;
    }

    public HeadBuilder setSkin(SkinTexture skin){
        if(this.itemStack.getItemMeta() != null && this.itemStack.getItemMeta() instanceof SkullMeta){
            if(skin != null){
                if(skin.getUrl() != null && !skin.getUrl().equals("null")){
                    SkullMeta meta = ((SkullMeta)this.itemStack.getItemMeta());
                    GameProfile gameProfile = new GameProfile(UUID.randomUUID(), "");
                    byte[] skinBytes = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", skin.getUrl()).getBytes());
                    gameProfile.getProperties().put("textures", new Property("textures", new String(skinBytes)));
                    try{
                        Field profile = meta.getClass().getDeclaredField("profile");
                        profile.setAccessible(true);
                        profile.set(meta, gameProfile);
                    }catch(Exception ex){
                        ex.printStackTrace();
                    }
                    this.itemStack.setItemMeta(meta);
                }
            }
        }
        return this;
    }

    public HeadBuilder setDisplayName(String displayName){
        if(this.itemStack.getItemMeta() != null && this.itemStack.getItemMeta() instanceof SkullMeta){
            SkullMeta meta = ((SkullMeta) this.itemStack.getItemMeta());
            meta.setDisplayName(Utils.ct(displayName));
            this.itemStack.setItemMeta(meta);
        }
        return this;
    }

    public HeadBuilder setLore(String... lore){
        if(this.itemStack.getItemMeta() != null && this.itemStack.getItemMeta() instanceof SkullMeta){
            SkullMeta meta = ((SkullMeta)this.itemStack.getItemMeta());
            meta.setLore(Utils.ct(Utils.toList(lore)));
            this.itemStack.setItemMeta(meta);
        }
        return this;
    }

    public HeadBuilder setAmount(int amount){
        this.itemStack.setAmount(amount);
        return this;
    }

    public HeadBuilder addEnchantment(Enchantment enchantment, int level){
        this.itemStack.addUnsafeEnchantment(enchantment,level);
        return this;
    }

    public HeadBuilder addEnchantment(Enchantment enchantment){
        return this.addEnchantment(enchantment,1);
    }

    public HeadBuilder showEnchantments(boolean show){
        if(this.itemStack.getItemMeta() != null && this.itemStack.getItemMeta() instanceof SkullMeta){
            SkullMeta meta = ((SkullMeta)this.itemStack.getItemMeta());
            if(show){
                meta.removeItemFlags(ItemFlag.HIDE_ENCHANTS);
            }else{
                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            }
            this.itemStack.setItemMeta(meta);
        }
        return this;
    }

    public HeadBuilder showAllAttributes(boolean show){
        if(this.itemStack.getItemMeta() != null && this.itemStack.getItemMeta() instanceof SkullMeta){
            SkullMeta meta = ((SkullMeta)this.itemStack.getItemMeta());
            if(show){
                meta.removeItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            }else{
                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            }
            this.itemStack.setItemMeta(meta);
        }
        return this;
    }

    public ItemStack construct(){
        return this.itemStack.clone();
    }
}
