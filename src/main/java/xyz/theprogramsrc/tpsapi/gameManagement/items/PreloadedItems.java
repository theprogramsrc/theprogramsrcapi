package xyz.theprogramsrc.tpsapi.gameManagement.items;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import xyz.theprogramsrc.tpsapi.TPS;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

@SuppressWarnings ("ALL")
public class PreloadedItems extends TPS{

    public PreloadedItems(TheProgramSrcClass theProgramSrcClass){
        super(theProgramSrcClass,false);
    }

    public ItemStack getBackItem(){
        return new ItemBuilder(XMaterial.ARROW).setDisplayName(getLanguageManager().getPhrase("preloadeditem-back-name")).setLore(Utils.toStringArray(getLanguageManager().getPhrase("preloadeditem-back-lore"))).setAmount(1).construct();
    }

    public ItemStack getNextItem(){
        return Utils.isConnected() ?
                (new HeadBuilder(XMaterial.PLAYER_HEAD, "MHF_ArrowRight")
                        .setDisplayName(getLanguageManager().getPhrase("preloadeditem-nextpage-name"))
                        .setLore(Utils.toStringArray(getLanguageManager().getPhrase("preloadeditem-nextpage-lore")))
                        .construct()) :
                (new ItemBuilder(XMaterial.ARROW)
                        .setDisplayName(getLanguageManager().getPhrase("preloadeditem-nextpage-name"))
                        .setLore(Utils.toStringArray(getLanguageManager().getPhrase("preloadeditem-nextpage-lore")))
                        .construct());
    }

    public ItemStack getPrevItem(){
        return Utils.isConnected() ?
                (new HeadBuilder(XMaterial.PLAYER_HEAD, "MHF_ArrowLeft")
                        .setDisplayName(getLanguageManager().getPhrase("preloadeditem-prevpage-name"))
                        .setLore(Utils.toStringArray(getLanguageManager().getPhrase("preloadeditem-prevpage-lore")))
                        .construct()) :
                (new ItemBuilder(XMaterial.ARROW)
                        .setDisplayName(getLanguageManager().getPhrase("preloadeditem-prevpage-name"))
                        .setLore(Utils.toStringArray(getLanguageManager().getPhrase("preloadeditem-prevpage-lore")))
                        .construct());
    }

    public ItemStack getBlankItem(){
        return new ItemBuilder(XMaterial.BLACK_STAINED_GLASS_PANE).setDisplayName("&7").setAmount(1).showAllAttributes(false).construct();
    }

    public ItemStack getSearchItem(){
        return new ItemBuilder(XMaterial.BOOKSHELF).setDisplayName(getLanguageManager().getPhrase("preloadeditem-search-name")).setLore(Utils.toStringArray(getLanguageManager().getPhrase("preloadeditem-search-lore"))).construct();
    }

    public ItemStack getEndSearchItem(){
        return new ItemBuilder(XMaterial.BOOKSHELF).setDisplayName(getLanguageManager().getPhrase("preloadeditem-endsearch-name")).setLore(Utils.toStringArray(getLanguageManager().getPhrase("preloadeditem-endsearch-lore"))).addEnchantment(Enchantment.DURABILITY).showEnchantments(false).construct();
    }

    public ItemStack getRefreshItem(){
        return new ItemBuilder(XMaterial.LIME_WOOL).setDisplayName(getLanguageManager().getPhrase("preloadeditem-refresh-name")).setLore(Utils.toStringArray(getLanguageManager().getPhrase("preloadeditem-refresh-lore"))).construct();
    }
}
