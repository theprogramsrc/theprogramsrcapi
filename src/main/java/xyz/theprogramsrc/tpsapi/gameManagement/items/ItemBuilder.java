package xyz.theprogramsrc.tpsapi.gameManagement.items;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;

@SuppressWarnings("ALL")
public class ItemBuilder{

    private ItemStack itemStack;

    public ItemBuilder(XMaterial xMaterial){
        this.itemStack = xMaterial.parseItem();
    }

    public ItemBuilder setDisplayName(String displayName){
        if(displayName != null){
            ItemMeta meta = this.itemStack.getItemMeta();
            meta.setDisplayName(Utils.ct(displayName));
            this.itemStack.setItemMeta(meta);
        }
        return this;
    }

    public ItemBuilder setLore(String... lore){
        if(lore != null){
            ItemMeta meta = this.itemStack.getItemMeta();
            meta.setLore(Utils.ct(Utils.toList(lore)));
            this.itemStack.setItemMeta(meta);
        }
        return this;
    }

    public ItemBuilder setAmount(int amount){
        this.itemStack.setAmount(amount);
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment enchantment, int level){
        this.itemStack.addUnsafeEnchantment(enchantment,level);
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment enchantment){
        return this.addEnchantment(enchantment,1);
    }

    public ItemBuilder showEnchantments(boolean show){
        ItemMeta meta = this.itemStack.getItemMeta();
        if(show){
            meta.removeItemFlags(ItemFlag.HIDE_ENCHANTS);
        }else{
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        this.itemStack.setItemMeta(meta);
        return this;
    }

    public ItemBuilder showAllAttributes(boolean show){
        ItemMeta meta = this.itemStack.getItemMeta();
        if(show){
            meta.removeItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        }else{
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        }
        this.itemStack.setItemMeta(meta);
        return this;
    }

    public ItemStack construct(){
        return this.itemStack.clone();
    }
}
