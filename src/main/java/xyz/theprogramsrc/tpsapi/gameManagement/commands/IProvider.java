package xyz.theprogramsrc.tpsapi.gameManagement.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public interface IProvider{

    Result onPlayerExecute(Player player, String[] args);

    Result onConsoleExecute(CommandSender sender, String[] args);

    String getCommand();

    String getUsage();

    String getDescription();

    List<String> getAliases();

    String getPermission();
}
