package xyz.theprogramsrc.tpsapi.gameManagement.commands;

public enum  Result{

    COMPLETED,
    INVALID_ARGS,
    NO_PERMISSION,
    NOT_SUPPORTED
}