package xyz.theprogramsrc.tpsapi.gameManagement.commands;

import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public abstract class ICommand extends BukkitCommand implements IProvider{

    private static TheProgramSrcClass tps;
    private String command;

    public ICommand(TheProgramSrcClass theProgramSrcClass, String command){
        super(command);
        tps = theProgramSrcClass;
        this.command = command;
        this.setAliases(this.getAliases());
        this.setUsage(this.getUsage());
        this.setDescription(this.getDescription());

        try {
            final Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            bukkitCommandMap.setAccessible(true);
            CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());
            commandMap.register(command, this);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings){
        Result r;
        if(commandSender instanceof Player){
            if(!this.getPermission().equalsIgnoreCase("None") && !commandSender.hasPermission(this.getPermission())){
                r = Result.NO_PERMISSION;
            }else{
                r = this.onPlayerExecute(((Player)commandSender), strings);
            }
        }else{
            r = this.onConsoleExecute(commandSender, strings);
        }
        this.sendResult(r,commandSender);
        return false;
    }

    private void sendResult(Result result, CommandSender sender){

        String lang = tps.getLanguage();
        String msg;
        switch(result){
            case INVALID_ARGS:
                msg = getTPS().getLanguageManager().getPhrase("commandapi-invalid-args");
                if(sender instanceof Player){
                    Utils.sendMessage(sender, tps.getPrefix().endsWith(" ") ? tps.getPrefix()+msg : tps.getPrefix()+" "+msg);
                }else{
                    tps.log(msg);
                }
                break;
            case NOT_SUPPORTED:
                msg = sender instanceof Player ? getTPS().getLanguageManager().getPhrase("commandapi-player-not-supported") : getTPS().getLanguageManager().getPhrase("commandapi-console-not-supported");
                if(sender instanceof Player){
                    Utils.sendMessage(sender, tps.getPrefix().endsWith(" ") ? tps.getPrefix()+msg : tps.getPrefix()+" "+msg);
                }else{
                    tps.log(msg);
                }
                break;
            case NO_PERMISSION:
                msg = getTPS().getLanguageManager().getPhrase("commandapi-no-permission");
                if(sender instanceof Player){
                    Utils.sendMessage(sender, tps.getPrefix().endsWith(" ") ? tps.getPrefix()+msg : tps.getPrefix()+" "+msg);
                }else{
                    tps.log(msg);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public String getUsage(){ return "/"+this.getCommand(); }

    @Override
    public String getCommand(){ return command; }

    @Override
    public List<String> getAliases(){ return new ArrayList<>(); }

    @Override
    public String getDescription(){ return getTPS().getLanguageManager().getPhrase("command")+" "+this.getUsage(); }

    public void addAlias(String alias){
        this.getAliases().add(alias);
    }

    public static TheProgramSrcClass getTPS(){ return tps; }

    @Override
    public String getPermission(){ return "None"; }
}
