package xyz.theprogramsrc.tpsapi.gameManagement.world;

import xyz.theprogramsrc.tpsapi.programmingUtils.zip.ZipUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;

import java.io.File;

@SuppressWarnings ("ALL")
public class iWorld {

    public static WorldCreator creator(String name){ return new WorldCreator(name); }

    public static WorldManager manager(World world){ return new WorldManager(world); }

    public static WorldDeleter deleter(World world){ return new WorldDeleter(world); }

    public static World create(String name, World.Environment environment, WorldType worldType, boolean generateStructures){ return creator(name).environment(environment).generateStructures(generateStructures).type(worldType).createWorld(); }

    private static class WorldManager{

        private World world;

        WorldManager(World world){
            this.world = world;
        }

        public WorldManager setName(String name){
            this.world = iWorld.creator(name).copy(this.world).createWorld();
            return this;
        }

        public WorldManager setSeed(Integer seed){
            this.world = iWorld.creator(this.world.getName()).copy(this.world).seed(Long.valueOf(seed.toString())).createWorld();
            return this;
        }
    }

    private static class WorldDeleter{

        private World world;

        WorldDeleter(World world){
            this.world = world;
        }

        public WorldDeleter safeDelete(File backupLocation){
            new ZipUtils(world.getWorldFolder(), backupLocation);
            return this;
        }

        public WorldDeleter unsafeDelete(){
            Bukkit.unloadWorld(this.world,false);
            return this;
        }
    }
}
