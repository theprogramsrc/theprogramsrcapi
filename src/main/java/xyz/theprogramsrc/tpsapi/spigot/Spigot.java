package xyz.theprogramsrc.tpsapi.spigot;

import xyz.theprogramsrc.tpsapi.TPS;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@SuppressWarnings("unused")
public class Spigot extends TPS{

    private String id;

    public Spigot(TheProgramSrcClass theProgramSrcClass, String resourceId){
        super(theProgramSrcClass, false);
        this.id = resourceId;
    }

    public boolean hasNewUpdate(){
        String apiUrl = "https://api.spigotmc.org/legacy/update.php?resource="+this.id;
        String latestVersion;
        String actualVersion = this.getPlugin().getDescription().getVersion();
        try{
            HttpURLConnection connection = ((HttpURLConnection)new URL(apiUrl).openConnection());
            connection.setReadTimeout(1250);
            connection.setConnectTimeout(1250);
            latestVersion = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();
            if(latestVersion.length() <= 8){
                if(!actualVersion.equals(latestVersion)){
                    return true;
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

}
