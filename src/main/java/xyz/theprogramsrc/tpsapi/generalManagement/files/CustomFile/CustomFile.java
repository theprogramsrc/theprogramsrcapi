package xyz.theprogramsrc.tpsapi.generalManagement.files.CustomFile;

import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;
import xyz.theprogramsrc.tpsapi.programmingUtils.exceptions.TPSException;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.*;
import xyz.theprogramsrc.tpsapi.TPS;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;

import java.io.File;
import java.util.*;

@SuppressWarnings("all")
public abstract class CustomFile extends TPS implements IFile{

    private FileConfiguration config;
    private File file;
    private boolean logMessages;
    private String fileName;

    public CustomFile(TheProgramSrcClass theProgramSrcClass, boolean load, boolean logMessages){
        super(theProgramSrcClass);
        Utils.notNull(this.getFileName(), getLanguageManager().getPhrase("file-name-cant-be-null"));
        Utils.notNull(this.getFileExtension(), getLanguageManager().getPhrase("file-extension-cant-be-null"));
        Utils.notNull(this.getFileFolder(), getLanguageManager().getPhrase("file-folder-cant-be-null"));
        this.logMessages = logMessages;
        this.fileName = this.getFileName() + (this.getFileExtension().contains(".") ? this.getFileExtension() : Utils.setFirstFor(".", this.getFileExtension()));
        this.file = new File((this.getFileFolder() != null ? this.getFileFolder() : this.getMainFolder()).getAbsolutePath(), this.fileName);
        this.loadFile(load, logMessages);
    }

    private void loadFile(boolean load, boolean logMessages){
        this.config = YamlConfiguration.loadConfiguration(this.file);
        this.options().copyDefaults(true);
        if(load){ this.onFileLoad(); }
        if(logMessages){
            if(this.file.exists()){
                this.log(getLanguageManager().getPhrase("file-loaded").replace("{FILE}", this.fileName));
            }else{
                this.log(getLanguageManager().getPhrase("file-created").replace("{FILE}", this.fileName));
            }
        }
    }

    public void reload(){
        this.onFileReload();
        this.config = YamlConfiguration.loadConfiguration(this.file);
        if(this.logMessages) this.log(getLanguageManager().getPhrase("file-reloaded").replace("{FILE}", this.fileName));
    }

    public void save(){
        try{
            this.config.save(this.file);
        }catch(Exception ex){
            throw new TPSException(ex);
        }
    }

    public void add(String path, Object value){ this.config.addDefault(path,value); }

    public void set(String path, Object value){ this.config.set(path,value); }

    public Object get(String path){ return this.config.get(path); }

    public String getString(String path){ return this.config.getString(path); }

    public Integer getInt(String path){ return this.config.getInt(path); }

    public Double getDouble(String path){ return this.config.getDouble(path); }

    public Long getLong(String path){ return this.config.getLong(path); }

    public Boolean getBoolean(String path){ return this.config.getBoolean(path); }

    public List<?> getList(String path){ return this.config.getList(path); }

    public List<String> getStringList(String path){ return this.config.getStringList(path); }

    public Set<String> getKeys(boolean deep){ return this.config.getKeys(deep); }

    public FileConfigurationOptions options(){ return this.config.options(); }

    public ConfigurationSection getConfigurationSection(String path){ return this.config.getConfigurationSection(path); }

    public boolean contains(String path){ return this.config.contains(path); }

    public void remove(String path){ this.config.set(path,null); }

    protected void setHeader(String... header){
        StringBuilder r = new StringBuilder();
        for(int i=0;i<header.length;++i){
            String s = header[i];
            r.append(s);
            if(i != 0){
                r.append("\n");
            }
        }
        this.options().header(r.toString());
    }

    public FileConfiguration getFileConfiguration(){ return this.config; }

    public File toFile(){ return this.file; }

    public void onFileLoad(){}

    public void onFileReload(){}

}
