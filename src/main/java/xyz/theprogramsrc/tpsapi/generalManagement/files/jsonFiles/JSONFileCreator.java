package xyz.theprogramsrc.tpsapi.generalManagement.files.jsonFiles;

import xyz.theprogramsrc.tpsapi.programmingUtils.json.JSONUtils;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import xyz.theprogramsrc.tpsapi.TPS;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;

import java.io.*;
import java.util.*;

public abstract class JSONFileCreator extends TPS{

    private HashMap<String, Object> cache;

    public JSONFileCreator(TheProgramSrcClass theProgramSrcClass){
        super(theProgramSrcClass,false);
        this.cache = new HashMap<>();
        this.onJSONFileLoad();
    }

    public void reload(){
        this.cache.clear();
        this.onJSONFileReload();
    }

    public abstract void onJSONFileLoad();

    public abstract void onJSONFileReload();

    public void save(){
        try{
            JSONUtils json = new JSONUtils(new JSONObject());
            this.cache.forEach(json::set);
            File f = new File(this.getFileFolder().getAbsolutePath(),this.getFileName().contains(".json") ? this.getFileName() : this.getFileName()+".json");
            if(f.exists()){ FileUtils.forceDelete(f); }
            FileWriter fileWriter = new FileWriter(f);
            fileWriter.write(json.serialize());
            fileWriter.flush();
            fileWriter.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void load(){
        try{
            File f = new File(this.getFileFolder().getAbsolutePath(),this.getFileName().contains(".json") ? this.getFileName() : this.getFileName()+".json");
            if(f.exists()){
                FileReader fileReader = new FileReader(f);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String json = bufferedReader.readLine();
                JSONUtils jsonUtils = new JSONUtils(json);
                this.cache.putAll(jsonUtils.getMap());
                this.save();
            }else{
                this.save();
                this.load();
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public String getJSONString(){
        JSONUtils jsonUtils = new JSONUtils(new JSONObject());
        jsonUtils.addAll(this.cache);
        return jsonUtils.serialize();
    }

    public JSONUtils getJSON(){
        JSONUtils jsonUtils = new JSONUtils(new JSONObject());
        jsonUtils.addAll(this.cache);
        return jsonUtils;
    }

    public void add(String key, Object value){
        this.cache.put(key,value);
    }

    public String getString(String key){ return ((String)this.get(key)); }

    public Boolean getBoolean(String key){ return Boolean.valueOf(this.getString(key)); }

    public Integer getInt(String key){ return Integer.parseInt(this.getString(key)); }

    public Object get(String key){
        if(!this.cache.containsKey(key)){
            this.save();
            this.load();
            this.cache.put(key,this.getJSON().get(key));
        }
        return this.cache.get(key);
    }

    public abstract String getFileName();

    public abstract File getFileFolder();
}
