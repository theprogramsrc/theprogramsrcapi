package xyz.theprogramsrc.tpsapi.generalManagement.files;

import xyz.theprogramsrc.tpsapi.TPS;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.programmingUtils.json.JSONUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Set;

@SuppressWarnings ({"RedundantExpression","unused"})
public class WebJSONReader extends TPS{

    private JSONUtils jsonUtils;

    public WebJSONReader(TheProgramSrcClass theProgramSrcClass, String jsonWebLocation){
        super(theProgramSrcClass);
        this.jsonUtils = new JSONUtils(readUrl(jsonWebLocation));
    }

    public String getString(String key){
        return this.jsonUtils.getString(key);
    }

    public Object get(String key){
        return this.jsonUtils.get(key);
    }

    public Integer getInt(String key){
        return this.jsonUtils.getInteger(key);
    }

    public Boolean getBoolean(String key){
        return this.jsonUtils.getBoolean(key);
    }

    public Set<String> getKeys(){
        return this.jsonUtils.getKeys();
    }

    public JSONUtils getArray(String key){
        return this.jsonUtils.getArray(key);
    }

    private static String readUrl(String urlString){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(new URL(urlString).openStream()));
            StringBuilder builder = new StringBuilder();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                builder.append(chars, 0, read);

            reader.close();
            return builder.toString();
        } catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
}
