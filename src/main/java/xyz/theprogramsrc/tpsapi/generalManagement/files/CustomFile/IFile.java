package xyz.theprogramsrc.tpsapi.generalManagement.files.CustomFile;

import java.io.File;

public interface IFile{

    void onFileLoad();

    void onFileReload();

    String getFileName();

    String getFileExtension();

    File getFileFolder();
}
