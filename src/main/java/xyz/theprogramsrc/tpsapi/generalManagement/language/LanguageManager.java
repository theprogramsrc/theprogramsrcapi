package xyz.theprogramsrc.tpsapi.generalManagement.language;

import org.bukkit.configuration.file.YamlConfiguration;
import xyz.theprogramsrc.tpsapi.TPS;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.generalManagement.files.WebJSONReader;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;
import xyz.theprogramsrc.tpsapi.programmingUtils.json.JSONUtils;

import java.io.File;

@SuppressWarnings ({"FieldCanBeLocal","unused","RedundantExpression"})
public class LanguageManager extends TPS{

    private String languageLocation;
    private WebJSONReader reader;
    private JSONUtils phrases;
    private String lang;

    public LanguageManager(TheProgramSrcClass theProgramSrcClass){
        super(theProgramSrcClass, false);
    }

    @Override
    public void onLoad(){
        if(this.isFirstStart()){
            this.languageLocation = "https://theprogramsrc.xyz/plugins/general/language_base_en.json";
            this.reader = new WebJSONReader(getTPS(), this.languageLocation);
            this.phrases = this.reader.getArray("phrases");
        }else{
            String lang = this.getlang();
            this.languageLocation = "https://theprogramsrc.xyz/plugins/general/language_base_"+lang+".json";
            this.reader = new WebJSONReader(getTPS(), this.languageLocation);
            this.phrases = this.reader.getArray("phrases");
        }
    }

    public String getLanguageAuthor(){
        return this.reader.getString("language-author");
    }

    public String getLanguageVersion(){
        return this.reader.getString("language-version");
    }

    public String getLanguageName(){
        return this.reader.getString("language-name");
    }

    public String getPhrase(String internalName){
        return this.isFirstStart() ? this.phrases.getString(internalName) : this.phrases.getString(internalName).replace("{PREFIX}", getprefix());
    }

    private String getlang(){
        YamlConfiguration config = YamlConfiguration.loadConfiguration(new File(this.getConfigFolder().getAbsolutePath()+"/config.yml"));
        return config.getString("Language");
    }

    private String getprefix(){
        YamlConfiguration config = YamlConfiguration.loadConfiguration(new File(this.getMainFolder().getAbsolutePath()+"/"+this.getPluginName()+".yml"));
        return config.getString("Prefix");
    }
}
