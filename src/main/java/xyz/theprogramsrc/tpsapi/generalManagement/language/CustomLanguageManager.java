package xyz.theprogramsrc.tpsapi.generalManagement.language;

import xyz.theprogramsrc.tpsapi.TPS;
import xyz.theprogramsrc.tpsapi.TheProgramSrcClass;
import xyz.theprogramsrc.tpsapi.generalManagement.files.WebJSONReader;
import xyz.theprogramsrc.tpsapi.programmingUtils.json.JSONUtils;

@SuppressWarnings("ALL")
public class CustomLanguageManager extends TPS{

    private String languageLocation;
    private WebJSONReader reader;
    private JSONUtils phrases;
    private String webLocation;

    public CustomLanguageManager(TheProgramSrcClass theProgramSrcClass, String webLocation){
        super(theProgramSrcClass);
        this.webLocation = webLocation;
        this.load();
    }

    public void load(){
        String lang = this.getLanguage();
        this.languageLocation = this.webLocation.replace("{LANGUAGE}", lang);
        this.reader = new WebJSONReader(getTPS(), this.languageLocation);
        this.phrases = this.reader.getArray("phrases");
    }

    public String getLanguageAuthor(){
        return this.reader.getString("language-author");
    }

    public String getLanguageVersion(){
        return this.reader.getString("language-version");
    }

    public String getLanguageName(){
        return this.reader.getString("language-name");
    }

    public String getPhrase(String internalName){
        return this.phrases.getString(internalName).replace("{PREFIX}", getPrefix());
    }
}
