package xyz.theprogramsrc.tpsapi;

import org.bukkit.entity.Player;
import xyz.theprogramsrc.tpsapi.gameManagement.items.PreloadedItems;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.SkinTexture;
import xyz.theprogramsrc.tpsapi.gameManagement.user.UserManager;
import xyz.theprogramsrc.tpsapi.generalManagement.language.LanguageManager;
import xyz.theprogramsrc.tpsapi.programmingUtils.runnable.CountdownTimer;
import xyz.theprogramsrc.tpsapi.ownOptions.files.Config;
import xyz.theprogramsrc.tpsapi.ownOptions.files.MySQLFile;
import xyz.theprogramsrc.tpsapi.ownOptions.files.PluginFile;
import xyz.theprogramsrc.tpsapi.programmingUtils.mysql.SQL;
import xyz.theprogramsrc.tpsapi.programmingUtils.mysql.SQLConnection;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.function.Consumer;

@SuppressWarnings("ALL")
public class TPS implements Listener{

    private static TheProgramSrcClass tps;

    public TPS(TheProgramSrcClass theProgramSrcClass, boolean registerListener){
        tps = theProgramSrcClass;
        if(registerListener) this.registerEvents(this);
        this.onLoad();
    }

    public TPS(TheProgramSrcClass theProgramSrcClass){ this(theProgramSrcClass,true); }

    public void onLoad(){}

    protected void registerEvents(Listener... listeners){
        for(Listener listener : listeners){
            Bukkit.getPluginManager().registerEvents(listener, this.getPlugin());
        }
    }

    protected void callEvents(Event... events){
        for(Event event : events){
            Bukkit.getPluginManager().callEvent(event);
        }
    }

    public String getPrefix(){ return tps.getPrefix(); }

    protected JavaPlugin getPlugin(){ return tps.getPlugin(); }

    protected File getMainFolder(){ return tps.getMainFolder(); }

    protected File getConfigFolder(){ return tps.getConfigFolder(); }

    protected File getDataFolder(){ return tps.getDataFolder(); }

    protected void log(String message){ tps.log(message); }

    protected void log(String english, String spanish){ tps.log(english,spanish); }

    protected String getAuthorName(){ return tps.getAuthorName(); }

    protected String getPluginName(){ return tps.getPluginName(); }

    public String getLanguage(){ return tps.getLanguage(); }

    public Config getConfig(){ return tps.getConfig(); }

    public PluginFile getPluginFile(){ return tps.getPluginFile(); }

    public static TheProgramSrcClass getTPS(){ return tps; }

    public boolean isSpanish(){ return tps.isSpanish(); }

    public SQLConnection getSQLConnection(){ return tps.getSQLConnection(); }

    public MySQLFile getSQLFile(){ return tps.getSQLFile(); }

    public SQL getSQL(){ return tps.getSQL(); }

    public boolean isPaid(){ return tps.isPaid(); }

    public boolean isDevMode(){ return tps.isDevMode(); }

    public boolean isDemoMode(){ return tps.isDemoMode(); }

    public boolean isFirstStart(){ return tps.isFirstStart(); }

    public void createCountdown(int seconds, Runnable beforeTimer, Runnable afterTimer, Consumer<CountdownTimer> everySecond){ tps.createCountdown(seconds,beforeTimer,afterTimer,everySecond); }

    public PreloadedItems getPreloadedItems(){ return tps.getPreloadedItems(); }

    public UserManager getUserManager(){ return tps.getUserManager(); }

    public SkinTexture getSkin(Player p){ return tps.getSkin(p); }

    public LanguageManager getLanguageManager(){ return tps.getLanguageManager(); }
}
