package xyz.theprogramsrc.tpsapi;

import com.massivestats.MassiveStats;
import org.bukkit.entity.Player;
import xyz.theprogramsrc.tpsapi.gameManagement.items.PreloadedItems;
import xyz.theprogramsrc.tpsapi.gameManagement.packets.packets.SkinTexture;
import xyz.theprogramsrc.tpsapi.gameManagement.user.UserManager;
import xyz.theprogramsrc.tpsapi.generalManagement.language.LanguageManager;
import xyz.theprogramsrc.tpsapi.ownOptions.commands.getUUIDCommand;
import xyz.theprogramsrc.tpsapi.programmingUtils.mysql.SQL;
import xyz.theprogramsrc.tpsapi.programmingUtils.mysql.SQLConnection;
import xyz.theprogramsrc.tpsapi.programmingUtils.runnable.CountdownTimer;
import xyz.theprogramsrc.tpsapi.programmingUtils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.event.*;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.theprogramsrc.tpsapi.ownOptions.files.Config;
import xyz.theprogramsrc.tpsapi.ownOptions.files.MySQLFile;
import xyz.theprogramsrc.tpsapi.ownOptions.files.PluginFile;

import java.io.File;
import java.util.function.Consumer;

@SuppressWarnings("ALL")
public class TheProgramSrcClass{

    private JavaPlugin javaPlugin;
    private String pluginName;
    private String authorName;

    /* Booleans */
    private boolean paid;
    private boolean devMode;
    private boolean demoMode;
    private boolean firstStart;

    /* Folders */
    private File mainFolder;
    private File configFolder;
    private File dataFolder;

    /* files */
    private Config config;
    private PluginFile pluginFile;

    /* Stats */
    private MassiveStats massiveStats;
    private boolean statsEnabled = false;

    /* SQL */
    private SQL sql;
    private MySQLFile sqlFile;
    private SQLConnection sqlConnection;

    /* Other */
    private PreloadedItems preloadedItems;

    /* UserAPI */
    private UserManager userManager;

    /* Language */
    private LanguageManager languageManager;


    public TheProgramSrcClass(JavaPlugin javaPlugin, String pluginName, String authorName, String pluginPrefix, String defaultLanguage, boolean userAPI, boolean paid, boolean devMode, boolean demoMode){
        this.javaPlugin = javaPlugin;
        this.pluginName = pluginName;
        this.authorName = authorName;
        this.paid = paid;
        this.devMode = devMode;
        this.demoMode = demoMode;
        this.mainFolder = this.javaPlugin.getDataFolder();
        this.firstStart = !this.mainFolder.exists();
        this.configFolder = new File(this.mainFolder.getAbsolutePath()+"/Configuration");
        this.configFolder.mkdir();
        this.dataFolder = new File(this.mainFolder.getAbsolutePath()+"/Data");
        this.dataFolder.mkdir();
        this.languageManager = new LanguageManager(this);
        this.config = new Config(this, defaultLanguage);
        this.pluginFile = new PluginFile(this,pluginPrefix);
        this.sqlFile = new MySQLFile(this);
        this.sqlConnection = new SQLConnection(this,"localhost",3306,"database","username","password");
        this.sql = new SQL(this.getSQLConnection());
        this.loadStats();
        this.preloadedItems = new PreloadedItems(this);
        new getUUIDCommand(this);
        if(userAPI){
            this.userManager = new UserManager(this);
            this.log(this.getLanguageManager().getPhrase("userapi-loaded"));
        }
        this.log(this.getLanguageManager().getPhrase("using-massivestats-system"));
        this.log(this.getLanguageManager().getPhrase("tpsapi-loaded"));
    }

    /* Setters and Loaders */

    public void setStatsEnabled(boolean enabled){
        this.statsEnabled = enabled;
        this.loadStats();
    }

    public void setPrefix(String prefix){ this.pluginFile.setDefaultPrefix(prefix); }

    public void loadStats(){
        if(this.massiveStats != null){
            if(!this.statsEnabled){
                this.massiveStats.stop();
            }
        }else{
            if(this.statsEnabled){
                this.massiveStats = new MassiveStats(this.javaPlugin);
            }
        }
    }


    /* Plugin */
    public JavaPlugin getPlugin(){ return this.javaPlugin; }

    /* Folders and files */

    public File getMainFolder(){ return this.mainFolder; }

    public File getConfigFolder(){ return this.configFolder; }

    public File getDataFolder(){ return this.dataFolder; }

    public Config getConfig(){ return this.config; }

    public PluginFile getPluginFile(){ return this.pluginFile; }

    public void reloadFiles(){
        this.pluginFile.reload();
        this.config.reload();
        this.userManager.reload();
    }

    /* Booleans */

    public boolean isPaid(){ return this.paid; }

    public boolean isDevMode(){ return this.devMode; }

    public boolean isDemoMode(){ return this.demoMode; }

    public boolean isFirstStart(){ return this.firstStart; }

    /* Strings */

    public String getAuthorName(){ return this.authorName; }

    public String getPluginName(){ return this.pluginName; }

    public String getLanguage(){ return this.isFirstStart() ? "en" : this.getConfig().getLanguage(); }

    public String getPrefix(){ return this.pluginFile.getPrefix(); }

    /* SQLConnection */

    public SQLConnection getSQLConnection(){ return this.sqlConnection; }

    public MySQLFile getSQLFile(){ return this.sqlFile; }

    public SQL getSQL(){ return this.sql; }


    /* Tasks */

    public void createCountdown(int seconds, Runnable beforeTimer, Runnable afterTimer, Consumer<CountdownTimer> everySecond){
        new CountdownTimer(this.javaPlugin, seconds, beforeTimer, afterTimer, everySecond).scheduleTimer();
    }

    /* Other Methods */

    public boolean isSpanish(){ return this.getLanguage().equalsIgnoreCase("es"); }

    public void log(String message){
        Utils.notNull(message, "Text to log Cannot be null!");
        this.javaPlugin.getServer().getConsoleSender().sendMessage(Utils.ct("&f&l"+this.pluginName+"&b&l>>&r "+message));
    }

    public void log(String spanish, String english){ this.log(this.isSpanish() ? spanish : english); }

    public void registerEvent(Listener... listeners) {
        Utils.notNull(listeners, "Listener Cannot be null!");
        for(Listener l : listeners){
            Bukkit.getPluginManager().registerEvents(l, this.getPlugin());
        }
    }

    public void callEvent(Event... events) {
        Utils.notNull(events, "Event Cannot be null!");
        for(Event e : events){
            Bukkit.getPluginManager().callEvent(e);
        }
    }

    public PreloadedItems getPreloadedItems(){
        return preloadedItems;
    }

    /* User */

    public UserManager getUserManager(){
        return userManager;
    }

    public SkinTexture getSkin(Player p){ return this.getPlugin().getServer().getOnlineMode() ? SkinTexture.fromPlayer(p) != null ? SkinTexture.fromPlayer(p) : SkinTexture.fromMojang("Steve") : SkinTexture.fromMojang(p.getName()) != null ? SkinTexture.fromMojang(p.getName()) : SkinTexture.fromMojang("Steve"); }

    /* Language */

    public LanguageManager getLanguageManager(){
        return languageManager;
    }
}
